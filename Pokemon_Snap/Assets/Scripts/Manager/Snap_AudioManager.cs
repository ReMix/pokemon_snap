﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Audio Manager - Controls audio and stuff
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class Snap_AudioManager : MonoBehaviour
    {
        #region Variables & Singleton
        /// <summary>
        /// Singleton Instance for having just one
        /// </summary>
        private static Snap_AudioManager instance;
        public static Snap_AudioManager Instance { get { return instance; } }

        /// <summary>
        /// Audio Types. More types can easily set
        /// </summary>
        public enum AudioType { AMBIENT, MUSIC, SFX, VOICE }

        /// <summary>
        /// Dictionary of AudioItems
        /// </summary>
        Dictionary<string, AudioItem> m_vAudioItems;
        /// <summary>
        /// List of removal keys. When needed, those are removed from AudioItem dictionary.
        /// </summary>
        List<string> m_vRemovabeDictionaryKeys;
        /// <summary>
        /// Manager Initialization checker
        /// </summary>
        bool m_bIsInit = false;

        [Header("AudioManager Settings"), Space(10)]
        /// <summary>
        /// Checker that updates the AudioItem Sources every frame in Update Method
        /// </summary>
        [SerializeField]
        private bool m_updateVolumesInRT = false;
        /// <summary>
        /// Checker that checks in every frame (Update Method) if we need to delete some keys
        /// </summary>
        [SerializeField] private bool m_removeKeysInRT = false;
        /// <summary>
        /// Checker that allows to destroy this instance when changing scenes
        /// </summary>
        [SerializeField] private bool destroyOnLoad = false;
        /// <summary>
        /// Main AudioSource. If we don't set this manually on inspector, we get this object's AudioSource
        /// </summary>
        [SerializeField] private AudioSource m_MainAudioSource;

        [Header("General Sources"), Space(10)]
        /// <summary>
        /// Master Volume - Every volume is TypeVolume * Master. Setting this to zero sets every source to zero.
        /// </summary>
        [SerializeField]
        [Range(0f, 1f)]
        float m_MasterVolume = 1f;
        /// <summary>
        /// Volume for Ambient Audio Items
        /// </summary>
        [SerializeField] [Range(0f, 1f)] float m_AmbientVolume = 1f;
        /// <summary>
        /// Volume for Music Audio Items
        /// </summary>
        [SerializeField] [Range(0f, 1f)] float m_MusicVolume = 1f;
        /// <summary>
        /// Volume for SFX Audio Items
        /// </summary>
        [SerializeField] [Range(0f, 1f)] float m_EffectsVolume = 1f;
        /// <summary>
        /// Volume for Voice Audio Items
        /// </summary>
        [SerializeField] [Range(0f, 1f)] float m_VoiceVolume = 1f;

        /// <summary>
        /// Properties for volumes
        /// </summary>
        public float VoiceVolume { set { m_VoiceVolume = value; } get { return m_VoiceVolume; } }
        public float AmbientVolume { set { m_AmbientVolume = value; } get { return m_AmbientVolume; } }
        public float MusicVolume { set { m_MusicVolume = value; } get { return m_MusicVolume; } }
        public float EffectsVolume { set { m_EffectsVolume = value; } get { return m_EffectsVolume; } }
        public float MasterVolume { set { m_MasterVolume = value; } get { return m_MasterVolume; } }
        /// <summary>
        /// Properties for changing things every frame (Update method)
        /// </summary>
        public bool UpdateVolumesInRealTime { set { m_updateVolumesInRT = value; } get { return m_updateVolumesInRT; } }
        public bool RemoveKeysInRealTime { set { m_removeKeysInRT = value; } get { return m_removeKeysInRT; } }

        [Header("Scene AudioSources Lists")]
        [SerializeField]
        private List<AudioItem> SceneAudioSources; //AudioItems que se encuentran en la escena siempre, los cuales 
        #endregion

        #region Init
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;

                m_vAudioItems = new Dictionary<string, AudioItem>();
                SceneAudioSources = new List<AudioItem>();
                m_vRemovabeDictionaryKeys = new List<string>();

                m_MasterVolume = PlayerPrefs.GetFloat("masterVolume", 1f);
                m_EffectsVolume = PlayerPrefs.GetFloat("sfxVolume", 1f);
                m_AmbientVolume = PlayerPrefs.GetFloat("ambientVolume", 1f);
                m_MusicVolume = PlayerPrefs.GetFloat("musicVolume", 1f);
                m_VoiceVolume = PlayerPrefs.GetFloat("voiceVolume", 1f);

                if (!destroyOnLoad)
                    DontDestroyOnLoad(this);
            }
            else
            {
                Debug.LogError("[UHMAudioManager]: Error! Solo puede haber una instancia de Audio Manager:" + gameObject.name + " con previa instancia: " + instance.gameObject.name);
                Destroy(this.gameObject);
            }
        }

        //Inicialize AudioManager creating a mainAudioSource in this.gameObject
        public void initAudioManager(AudioSource audioSource = null)
        {
            if (SceneAudioSources != null)
            {
                for (int i = 0; i < SceneAudioSources.Count; i++)
                {
                    if (!m_vAudioItems.ContainsKey(SceneAudioSources[i].itemName))
                    {
                        m_vAudioItems.Add(SceneAudioSources[i].itemName, SceneAudioSources[i]);

                        if (SceneAudioSources[i].isPlayedOnInit)
                            SceneAudioSources[i].PlayOnInit();
                    }
                }
            }

            if (!audioSource)
                m_MainAudioSource = GetComponent<AudioSource>();
            else
                m_MainAudioSource = audioSource;

            m_bIsInit = true;
        }

        #endregion

        #region Class AudioItem (SERIALIZABLE OBJECT)
        //This class has everything that can modify audiosource values (And audios
        [System.Serializable]
        private class AudioItem
        {
            [SerializeField] AudioSource audioSource;
            [SerializeField] AudioClip myClip;
            [SerializeField] [Range(0f, 1f)] float volume = 1f;
            [SerializeField] string sourceName;
            [SerializeField] AudioType Type = AudioType.SFX;
            [SerializeField] bool m_playedOnInit = false;
            [SerializeField] bool m_overrideSourceClip = false;
            [SerializeField] bool m_overrideVolume = false;

            bool bPaused = false;
            bool bAddedOnInit = false;

            public AudioClip MyClip { get { return myClip; } set { myClip = value; } }
            public AudioSource source { get { return audioSource; } }
            public string itemName { get { return sourceName; } }
            public float Volume { get { return volume; } set { volume = value; } }
            public bool isPaused { get { return bPaused; } set { bPaused = value; } }
            public bool isPlayedOnInit { get { return m_playedOnInit; } set { m_playedOnInit = value; } }
            public bool OverrideSourceClip { get { return m_overrideSourceClip; } set { m_overrideSourceClip = value; } }
            public bool OverrideVolume { get { return m_overrideVolume; } set { m_overrideVolume = value; } }
            public bool isAddedOnInit { get { return bAddedOnInit; } }
            public AudioType AudioType { get { return Type; } }

            //Inicializa un nuevo AudioItem por código
            public AudioItem(AudioSource source, AudioType type, string name, AudioClip clip = null, bool loop = false, float volume = 1, bool playedOnInit = false, bool overrideClip = false, bool overrideVolume = false)
            {
                m_overrideVolume = overrideVolume;

                if (m_overrideVolume)
                    this.volume = volume;
                else
                    this.volume = source.volume;

                this.sourceName = name;
                audioSource = source;
                Type = type;
                bPaused = false;
                source.loop = loop;
                myClip = clip;
                m_playedOnInit = playedOnInit;
                m_overrideSourceClip = overrideClip;

                if (overrideClip)
                {
                    if (clip != null)
                        source.clip = clip;
                }

                if (source.clip != null && myClip == null)
                    myClip = source.clip;

                UpdateVolume();

                if (audioSource.clip != null && m_playedOnInit)
                    audioSource.Play();
                else
                    audioSource.Stop();
            }

            //Inicializa el AudioItem si este fue asignado en  la escena.
            public void PlayOnInit()
            {
                if (source == null || sourceName == "")
                {
                    Debug.LogError("[UHMAudioItem] Error!! One audio item doesn't have a source or a name assigned in RunFirst");
                    return;
                }
                bAddedOnInit = true;

                if (myClip != null && (source.clip == null || (source.clip && m_overrideSourceClip)))
                    source.clip = myClip;
                else if (myClip == null && source.clip != null)
                    myClip = source.clip;

                UpdateVolume();

                if (m_playedOnInit)
                {
                    source.Play();
                }
                else
                {
                    bPaused = true;
                    source.Stop();
                }
            }

            //Cambia el clip del AudioSource y lo reproduce
            public void ChangeClipAndPlay(bool loop, AudioClip clip, float volume)
            {
                this.volume = volume;
                source.loop = loop;
                source.clip = clip;
                UpdateVolume();
                source.Play();
            }

            //Actualiza el volumen del AudioSource en funcion de los ajustes del jugador.
            public void UpdateVolume()
            {
                if (!source)
                    return;

                source.volume = volume;

                if (instance)
                {
                    float typeVolume = 1;

                    if (Type == AudioType.SFX)
                        typeVolume = instance.EffectsVolume;

                    else if (Type == AudioType.AMBIENT)
                        typeVolume = instance.AmbientVolume;

                    else if (Type == AudioType.MUSIC)
                        typeVolume = instance.MusicVolume;

                    else if (Type == AudioType.VOICE)
                        typeVolume = instance.VoiceVolume;

                    source.volume = volume * typeVolume * instance.MasterVolume;
                }
            }
        }
        #endregion

        #region MonoBehaviour
        void Update()
        {
            if (!m_bIsInit)
                return;

            if (m_updateVolumesInRT)
                UpdateAudioVolume();

            if (m_removeKeysInRT)
                RemoveKeys();
        }
        #endregion

        #region Add and Remove Stuff from Lists and Dictionaries
        /// <summary>
        /// Clear the audioItem list (as they are set when changing the scene). This is not needed
        /// </summary>
        public void ClearForChangingScene()
        {
            SceneAudioSources.Clear();
            m_vAudioItems.Clear();
        }

        /// <summary>
        /// Remove keys from Dictionary
        /// </summary>
        public void RemoveKeys()
        {
            for (int i = 0; i < m_vRemovabeDictionaryKeys.Count; i++)
            {
                string key = m_vRemovabeDictionaryKeys[i];
                if (m_vAudioItems.ContainsKey(key))
                {
                    if (m_vAudioItems[key].isAddedOnInit)
                    {
                        m_vRemovabeDictionaryKeys.Remove(key);
                        continue;
                    }

                    if (!m_vAudioItems[key].isPaused && !m_vAudioItems[key].source.isPlaying)
                    {
                        RemoveAudioPlaying(key);
                    }
                }
            }
        }

        /// <summary>
        /// Add an audioSource to the Dictionary
        /// </summary>
        /// <param name="source"></param>
        /// <param name="sourceName"></param>
        /// <param name="type"></param>
        /// <param name="audio"></param>
        /// <param name="loop"></param>
        /// <param name="volume"></param>
        /// <param name="playedOnInit"></param>
        /// <param name="overrideClip"></param>
        /// <param name="instantGo"></param>
        /// <param name="overrideVolume"></param>
        public void AddAudioSource(AudioSource source, string sourceName, AudioType type, AudioClip audio = null, bool loop = false, float volume = 1f, bool playedOnInit = false, bool overrideClip = false, bool instantGo = false, bool overrideVolume = false)
        {
            if (!m_vAudioItems.ContainsKey(sourceName))
            {
                AudioItem ai = new AudioItem(source, type, sourceName, audio, loop, volume, playedOnInit, overrideClip, overrideVolume);
                SceneAudioSources.Add(ai);
                m_vAudioItems.Add(sourceName, ai);
            }
            else
            {
                if (instantGo)
                {
                    // Recursividad para poder añadirlo 
                    AddAudioSource(source, sourceName + "1", type, audio, loop, volume, playedOnInit, overrideClip, instantGo, overrideVolume);
                    //Debug.Log("Recursividad en "+sourceName);
                }
                else
                {
                    Debug.LogError("[UHMAudioManager] Error!! The AudioItem " + sourceName + " already exists in the dictionary");
                }
            }
        }

        /// <summary>
        /// Deletes an audio from scene and dictionaty
        /// </summary>
        /// <param name="clipName"></param>
        public void RemoveAudioPlaying(string clipName)
        {
            if (m_vAudioItems.ContainsKey(clipName))
            {
                if (SceneAudioSources.Contains(m_vAudioItems[clipName]))
                {
                    SceneAudioSources.Remove(m_vAudioItems[clipName]);
                }

                Destroy(m_vAudioItems[clipName].source);
                m_vAudioItems.Remove(clipName);
            }

            if (m_vRemovabeDictionaryKeys.Contains(clipName))
                m_vRemovabeDictionaryKeys.Remove(clipName);
        }

        #endregion

        #region Playing Audio Methods
        /// <summary>
        /// Plays an Audio by Clip
        /// </summary>
        /// <param name="clip"></param>
        public void PlayAudio(AudioClip clip)
        {
            if (clip == null)
            {
                Debug.LogError("[UHMAudioManager] PlayAudio error, the param clip is null");
                return;
            }

            m_MainAudioSource.PlayOneShot(clip, 1f * m_EffectsVolume * m_MasterVolume);
        }

        /// <summary>
        /// Plays an audio by name (loaded from Resources)
        /// </summary>
        /// <param name="clipName"></param>
        public void PlayAudioByName(string clipName)
        {
            PlayAudio((Resources.Load("Sounds/" + clipName)) as AudioClip);
        }

        /// <summary>
        /// Makes an specific audio item to play
        /// </summary>
        /// <param name="key"></param>
        public void PlayAudioItem(string key, bool replaceClip = false)
        {
            if (key != null && key != "" && key != " ")
            {
                if (m_vAudioItems.ContainsKey(key))
                {
                    if (replaceClip)
                        m_vAudioItems[key].source.clip = m_vAudioItems[key].MyClip;

                    m_vAudioItems[key].source.Play();
                }
            }

        }
        /// <summary>
        /// Makes an specific audio item to stop
        /// </summary>
        /// <param name="key"></param>
        public void StopAudioItem(string key)
        {
            if (key != null && key != "" && key != " ")
            {
                if (m_vAudioItems.ContainsKey(key))
                {
                    m_vAudioItems[key].source.Stop();
                }
            }

        }

        /// <summary>
        /// Plays an audio using a previous source for it or creating a new one
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="clipName"></param>
        /// <param name="loop"></param>
        /// <param name="type"></param>
        /// <param name="volume"></param>
        public void PlayAudioStoppable(AudioClip clip, string clipName, bool loop = false, AudioType type = AudioType.SFX, float volume = 1f)
        {
            if (clip == null || m_vAudioItems == null)
            {
                Debug.LogError("[UHMAudioManager] PlayAudioStoppable error, the param clip is null <name>: " + clipName);
                return;
            }

            if (m_vAudioItems.ContainsKey(clipName))
            {
                m_vAudioItems[clipName].ChangeClipAndPlay(loop, clip, volume);

                if (!m_vRemovabeDictionaryKeys.Contains(clipName) && !loop)
                {
                    m_vRemovabeDictionaryKeys.Add(clipName);
                }
                else if (m_vRemovabeDictionaryKeys.Contains(clipName) && loop)
                {
                    m_vRemovabeDictionaryKeys.Remove(clipName);
                }
            }
            else
            {
                AudioItem newAudioItem = new AudioItem(this.gameObject.AddComponent<AudioSource>(), type, clipName, clip, loop, volume);
                m_vAudioItems.Add(clipName, newAudioItem);

                if (!loop)
                {
                    m_vRemovabeDictionaryKeys.Add(clipName);
                }
            }
        }

        public void PlayAudioStoppableByName(string clipName, bool loop = false, AudioType type = AudioType.SFX, float volume = 1f)
        {
            PlayAudioStoppable((Resources.Load("Sounds/" + clipName)) as AudioClip, clipName, loop, type, volume);
        }

        /// <summary>
        /// Plays an audio at certain point (3D sound)
        /// </summary>
        public void PlayAudioAtPoint()
        {
            //ToDo: code this
        }
        /// <summary>
        /// Plays an audio at certain point (3D sound) loaded by name
        /// </summary>
        public void PlayAudioAtPointByName()
        {
            //ToDo: code this
        }


        /// <summary>
        /// Pause an audio
        /// </summary>
        /// <param name="clipName"></param>
        public void PauseAudioPlaying(string clipName)
        {
            if (m_vAudioItems.ContainsKey(clipName))
            {
                if (!m_vAudioItems[clipName].isPaused)
                {
                    m_vAudioItems[clipName].source.Pause();
                    m_vAudioItems[clipName].isPaused = true;
                }
                else
                {
                    Debug.LogError("[UHMAudioManager] El audio " + clipName + " ya está pausado");
                }
            }
            else
            {
                Debug.LogError("[UHMAudioManager] El audio " + clipName + " no existe en el diccionario (PauseAudioPlaying)");
            }
        }

        /// <summary>
        /// Continua reproduciendo un audio o lo crea si no existe
        /// </summary>
        /// <param name="clipName"></param>
        /// <param name="loop"></param>
        /// <param name="type"></param>
        public void ContinueAudioPlaying(string clipName, bool loop = false, AudioType type = AudioType.SFX)
        {
            if (m_vAudioItems.ContainsKey(clipName) && m_vAudioItems[clipName].isPaused)
            {
                m_vAudioItems[clipName].isPaused = false;
                m_vAudioItems[clipName].source.Play();
            }
            else if (!m_vAudioItems.ContainsKey(clipName))
            {
                PlayAudioStoppableByName(clipName, loop, type);
            }
        }

        #endregion

        #region Test Stuff
        /// <summary>
        /// Set pitch to 3. Testing stuff
        /// </summary>
        public void setPitchAmbiental()
        {
            m_MainAudioSource.pitch = 3f;
        }
        /// <summary>
        /// Set pitch to normal (1). Testing stuff
        /// </summary>
        public void setPitchAmbientalNormal()
        {
            m_MainAudioSource.pitch = 1f;
        }
        #endregion

        #region Audio Check, Modify and Update
        /// <summary>
        /// Returns if the audio is being Playing and instantiated
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsAudioInstantiatedAndPlaying(string key)
        {
            if (m_vAudioItems.ContainsKey(key) && !m_vAudioItems[key].isPaused)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// If an audio is paused
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsAudioCurrentlyPaused(string key)
        {
            if (m_vAudioItems.ContainsKey(key))
            {
                if (m_vAudioItems[key].isPaused)
                    return true;
            }
            return false;
        }

        //Actualiza el volumen de todas las audioSources
        public void UpdateAudioVolume()
        {
            if (m_vAudioItems == null)
                return;

            foreach (AudioItem item in m_vAudioItems.Values)
            {
                item.UpdateVolume();
            }

            if (m_MainAudioSource != null)
                m_MainAudioSource.volume = m_EffectsVolume * m_MasterVolume;

        }


        /// <summary>
        /// Method that sets manually all the volumes 
        /// </summary>
        /// <param name="master"></param>
        /// <param name="sfx"></param>
        /// <param name="ambient"></param>
        /// <param name="music"></param>
        /// <param name="voices"></param>
        public void SetVolumes(float master = 1f, float sfx = 1f, float ambient = 1f, float music = 1f, float voices = 1f)
        {
            m_MasterVolume = master;
            m_EffectsVolume = sfx;
            m_AmbientVolume = ambient;
            m_MusicVolume = music;
            m_VoiceVolume = voices;
            UpdateAudioVolume();
        }

        /// <summary>
        /// Set new volume for a certain audioitem
        /// </summary>
        /// <param name="key"></param>
        /// <param name="volume"></param>
        public void SetSourceVolume(string key, float volume)
        {
            if (m_vAudioItems.ContainsKey(key))
            {
                m_vAudioItems[key].Volume = volume;
                m_vAudioItems[key].UpdateVolume();
            }
        }

        /// <summary>
        /// Modify the volume (plus, minus) of a certain audioitem
        /// </summary>
        /// <param name="key"></param>
        /// <param name="Mod"></param>
        public void ModifyVolume(string key, float Mod)
        {
            if (m_vAudioItems.ContainsKey(key))
            {
                m_vAudioItems[key].Volume += Mod;
                m_vAudioItems[key].UpdateVolume();
            }
        }

        /// <summary>
        /// Get the volume from an AudioItem
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public float GetVolume(string key)
        {
            float vol = -1f;
            if (m_vAudioItems.ContainsKey(key))
            {
                vol = m_vAudioItems[key].Volume;
            }
            return vol;
        }

        #endregion
    }

}