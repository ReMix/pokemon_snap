﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.PostProcessing;
using Fungus;

namespace PokemonSnapUnity
{

    /// <summary>
    /// Photo Manager
    /// </summary>
    public class Snap_PhotoManager : MonoBehaviour
    {
        #region Singleton
        /// <summary>
        /// Singleton Instance for having just one
        /// </summary>
        private static Snap_PhotoManager instance;
        public static Snap_PhotoManager Instance { get { return instance; } }
        #endregion

        #region Variables

        public enum PHOTO_HUD { NORMAL, POINTING, POINT };
        public enum SELECTION_MODE { NONE, OAK, ALBUM }

        [Header("~ PhotoManager Settings ~"), Space(10)]
        [SerializeField] private bool m_debug = true;
        [SerializeField] private bool m_destroyOnLoad = true;
        [SerializeField] private bool m_gainPointsWithPostProccessing = true;
        [SerializeField] private string m_photoFolder;
        [SerializeField] private string m_photoFolderName = "TempPics";
        [SerializeField] private string m_albumFolder;
        [SerializeField] private string m_albumFolderName = "AlbumPics";
        [SerializeField] private string m_stage = "Beach";
        [SerializeField] private bool m_pointing = false;
        [SerializeField] private int m_maxNumOfPhotos = 60;
        [SerializeField] private int m_minNumOfPhotos = 0;
        [SerializeField] private int m_numOfPhotos;
        [SerializeField] private Text m_numPhotoText;

        [Header("~ Camera Sounds ~"), Space(10)]
        [SerializeField] private AudioSource m_AudioSource;
        [SerializeField] private AudioClip m_cameraInSound;
        [SerializeField] private AudioClip m_cameraOutSound;
        [SerializeField] private AudioClip m_snapShotSound;
        [SerializeField] private AudioClip m_pointSound;
        [SerializeField] private AudioSource m_bgmSource;
        [SerializeField] private AudioClip m_endOfFilmSound;
        [SerializeField] private AudioClip m_evaluationTheme;
        [SerializeField] private AudioClip m_evaluationSFX;
        [SerializeField] private bool m_playedPointSound = false;

        [Header("~ Camera HUD Sprites ~"), Space(10)]
        [SerializeField]
        private Texture2D m_tCross;
        [SerializeField] private Texture2D[] icons;

        [Header("~ Camera HUD Settings ~"), Space(10)]
        [SerializeField] private bool m_hudVisible = true;
        [SerializeField] private PHOTO_HUD m_hudType = PHOTO_HUD.NORMAL;
        [SerializeField] private float m_sizeX = 50f;
        [SerializeField] private float m_sizeY = 50f;
        [SerializeField] private float m_normalHUDSize = 50f;
        [SerializeField] private float m_pointHUDSize = 70f;

        [Header("~ Saved Photos List ~"), Space(10)]
        [SerializeField] private GameObject m_blackScreen;
        [SerializeField] private List<Snap_Photo> m_photoList;
        [SerializeField] private GameObject m_photoBase;
        [SerializeField] private GameObject m_evaluatePhotoPanel;

        [Header("~ Other ~"), Space(10)]
        [SerializeField] private Camera m_Camera;
        [SerializeField] private float m_detectDistance = 15f;
        [SerializeField] private LayerMask m_layerMaskInteract = 8;
        [SerializeField] private Color RayGizmoColor;
        [SerializeField] private GameObject hitObject;
        [SerializeField] private Snap_PlayerController m_playerScript;
        [SerializeField] private GameObject m_normalIconGroup;
        [SerializeField] private GameObject m_photoIcon;
        [SerializeField] private GameObject m_endPanelNoPics;
        [SerializeField] private GameObject m_photoSelectionPanel;
        [SerializeField] private GameObject m_chooseGroup;

        [Header("~ Zoom ~"), Space(10)]
        [SerializeField] private float m_zoomTolerance = .1f;
        [SerializeField] private float m_normalFov = 60f;
        [SerializeField] private float m_zoomFov = 50f;
        [SerializeField] private float zoomSmooth = 2f;
        [SerializeField] private bool m_isZoomed = false;
        [SerializeField] private bool m_zooming = false;
        [SerializeField] private bool m_zoomingOut = false;
        [SerializeField] private bool m_canZoom = true;
        [SerializeField] private bool m_reachedLimit = false;

        [Header("~ ScreenShot ~"), Space(10)]
        [SerializeField] private Animator m_photoIconAnimator;
        [SerializeField] private string m_snapTrigger = "snapshot";
        [SerializeField] private float m_timeBetweenPics = .3f;
        [SerializeField] private bool m_usingScreenCapture = false;
        [SerializeField] private bool m_usePostProcessing = true;
        [SerializeField] private bool m_useMotionBlur = true;
        [SerializeField] private PostProcessingBehaviour m_PostProBehaviour;
        [SerializeField] private PostProcessingProfile m_PostProProfile;
        [SerializeField] private bool m_processingScreenshot = false;

        [Header("~ Data ~"), Space(10)]
        private string m_pokemonKey;
        private string m_pokemonName;
        private float m_distanceToPokemon;
        private float m_poseMultiplier;
        private float m_distanceToCenter;
        private float m_bonusPts;
        private bool m_moreThanOne;
        private bool m_samePokemon;
        private bool m_specialBonus;
        private bool m_newPokemon;
        private bool m_isSignal;

        [Header("~ End Course ~"), Space(10)]
        [SerializeField] private float m_waitedTimeToEvaluate = 5f;
        [SerializeField] private GameObject m_cameraCheckGroup;
        [SerializeField] private GameObject m_panelAllPhotos;
        [SerializeField] private GameObject m_panelSelectPhotos;
        [SerializeField] private RawImage m_selectedTexture;
        [SerializeField] private RawImage m_previousTexture;
        [SerializeField] private GameObject m_newBigIconGO;
        [SerializeField] private GameObject m_confirmSelection;
        [SerializeField] private GameObject m_previousNewGO;
        [SerializeField] private GameObject m_previousOakGO;
        [SerializeField] private GameObject m_previousAlbumGO;
        [SerializeField] private GameObject m_evaluationGO;
        [SerializeField] private GameObject m_groupSelectedPicsGO;
        [SerializeField] private GameObject m_panelSelectedPicsGO;
        [SerializeField] private Animator m_discoveryAnimator;
        [SerializeField] private string m_discoveryTrigger = "normal";
        [SerializeField] private string m_discoveryNewTrigger = "new";
        [SerializeField] private Button m_oakMarkBtn;
        [SerializeField] private Button m_albumMarkBtn;

        [Header("~ PhotoSelection Course ~"), Space(10)]
        [SerializeField] private Text m_msgBoxText;
        [SerializeField] private Text m_pokemonNameSelection;
        [SerializeField] private Text m_courseSelection;
        [SerializeField] private Text m_courseNameSelection;
        [SerializeField] private Text m_photoBy;
        [SerializeField] private Text m_TrainerName;
        [SerializeField] private Text m_IChooseThisText;
        [SerializeField] private Text m_oaksMarkButton;
        [SerializeField] private Text m_albumMarkButton;
        [SerializeField] private Text m_toProfOakButton;
        [SerializeField] private Text m_newBigIcon;
        [SerializeField] private Text m_cameraCheckText;
        [SerializeField] private Text m_cameraCheckCourseText;
        [SerializeField] private Text m_cameraCheckNameText;
        [SerializeField] private Text m_BackAllPhotos;
        [SerializeField] private Text m_outText;
        [SerializeField] private Text m_outOfFilmText;
        [SerializeField] private Text m_instructionsSelectionText;
        [SerializeField] private Text m_confirmMsgText;
        [SerializeField] private Text m_confirmYesText;
        [SerializeField] private Text m_confirmNoText;
        [SerializeField] private Text m_newInfoDiscover;
        [SerializeField] private Text m_discoveryName;

        [SerializeField] private Text m_profCheckTitle;
        [SerializeField] private Text m_thisTimeText;
        [SerializeField] private Text m_oldPicText;
        [SerializeField] private Text m_newEvaluationText;
        [SerializeField] private Text m_previewPhotoEvaluationText;

        [SerializeField] private Text m_sizeEvaluationText;
        [SerializeField] private Text m_poseEvaluationText;
        [SerializeField] private Text m_techEvaluationText;
        [SerializeField] private Text m_sameBonusEvaluationText;
        [SerializeField] private Text m_specialBonusEvaluationText;
        [SerializeField] private Text m_totalPtsEvaluationText;

        [SerializeField] private Flowchart m_flowChart;
        [SerializeField] private RawImage m_newImageTexture;
        [SerializeField] private RawImage m_oldImageTexture;
        [SerializeField] private Localization m_localization;
        [SerializeField] private GameObject m_previewSelectionGO;
        [SerializeField] private RawImage m_previewSelectionImg;

        [SerializeField] private AudioSource m_buttonSource;
        [SerializeField] private AudioClip m_buttonEnter;
        [SerializeField] private AudioClip m_buttonConfirm;
        [SerializeField] protected string m_snapShotTriggerEvent = "snapshot";
        [SerializeField] private List<Snap_Pokemon> m_pokemonList;

        private Snap_Photo m_currentPhoto;
        private Dictionary<string, Snap_Photo> m_oakPhotos;
        private Dictionary<string, Snap_Photo> m_albumPhotos;
        private List<string> m_selectedKeys;
        private List<GameObject> m_selectedPicsGO;
        [SerializeField] private bool m_showSelectedPics = true;

        private bool m_canPhotoSignal = false;
        private bool m_tutorialCompleted = false;
        int m_counter = 0;
        Texture2D m_texSheet;

        private Ray ray;
        private RaycastHit hit;
        public Snap_PlayerController PlayerScript { get { return m_playerScript; } }
        public bool HudVisible { get { return m_hudVisible; } set { m_hudVisible = value; } }
        public bool ProcessingPhoto { get { return m_processingScreenshot; } }
        public bool Pointing { get { return m_pointing; } }
        public string SnapTriggerEvent { get { return m_snapShotTriggerEvent; } }
        #endregion

        #region Monobehaviour Methods
        /// <summary>
        /// Awake. We initialize everything (And singleton instance)
        /// </summary>
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;

                if (!m_destroyOnLoad)
                    DontDestroyOnLoad(this);

                m_oakPhotos = new Dictionary<string, Snap_Photo>();
                m_albumPhotos = new Dictionary<string, Snap_Photo>();
                m_selectedKeys = new List<string>();
                m_selectedPicsGO = new List<GameObject>();
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        // Use this for initialization
        void Start()
        {
            CreatePhotoFolder();
            CreateAlbumFolder();

            DeletePics();

            m_numOfPhotos = m_maxNumOfPhotos;

            m_hudType = PHOTO_HUD.NORMAL;
            m_tCross = icons[0];
            m_sizeX = m_normalHUDSize;
            m_sizeY = m_normalHUDSize;

            if (!m_Camera)
                m_Camera = Camera.main;

            if (!m_AudioSource)
                m_AudioSource = GetComponent<AudioSource>();

            if (!m_PostProBehaviour)
                m_PostProBehaviour.GetComponent<PostProcessingBehaviour>();

            if (!m_usePostProcessing)
                m_PostProBehaviour.enabled = false;

            m_tutorialCompleted = Snap_SaveManager.Instance.GetGameData.m_tutorialComplete;
            m_canPhotoSignal = Snap_SaveManager.Instance.GetGameData.m_unlockedSignals;

            ParseTexts();
        }

        // Update is called once per frame
        void Update()
        {
            if (m_debug)
            {
                if (Input.GetKeyDown(KeyCode.F1))
                {
                    m_PostProBehaviour.enabled = !m_PostProBehaviour.enabled;
                }

                if (Input.GetKeyDown(KeyCode.F2))
                {
                    m_PostProProfile.motionBlur.enabled = !m_PostProProfile.motionBlur.enabled;
                }

                if (Input.GetKeyDown(KeyCode.F3))
                {
                    EndNoMorePics();
                }
            }

            if (!Snap_MenuManager.m_pauseGame && !Snap_MenuManager.m_initialAnimation && !Snap_MenuManager.m_endGame)
            {
                if (m_pointing)
                {
                    if (m_canZoom)
                    {
                        if (m_zooming)
                        {
                            CameraZoomIn();
                        }
                    }

                    if (Snap_InputManager.Instance.SnapInput())
                    {
                        if (!m_processingScreenshot)
                        {
                            StopAllCoroutines();
                            StartCoroutine(TakeScreenShotBufferPNG());
                        }

                    }

                    if (Snap_InputManager.Instance.PointUpInput())
                    {
                        if (m_canZoom)
                        {
                            m_photoIcon.SetActive(false);
                            m_normalIconGroup.SetActive(true);
                            m_isZoomed = false;
                            m_zooming = false;
                            m_zoomingOut = true;
                        }

                        m_AudioSource.PlayOneShot(m_cameraOutSound);
                        m_pointing = false;
                        return;
                    }

                    Vector3 position = new Vector3(Screen.width / 2, Screen.height / 2, m_Camera.transform.position.z);
                    Vector3 direction = transform.TransformDirection(Vector3.forward);
                    Debug.DrawRay(transform.position, direction * m_detectDistance, RayGizmoColor);

                    //Rayo a castear desde el centro de la cámara
                    /*m_Camera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, m_Camera.transform.position.z));*/
                    ray = new Ray(m_Camera.transform.position, m_Camera.transform.forward);

                    if (Physics.Raycast(ray, out hit, m_detectDistance, m_layerMaskInteract.value))
                    {
                        hitObject = hit.transform.gameObject;

                        if (hitObject.layer == LayerMask.NameToLayer("Interact"))
                        {
                            if (hitObject.tag == "Pokemon" || hitObject.tag == "Signal")
                            {
                                if (!m_playedPointSound)
                                {
                                    m_playedPointSound = true;
                                    m_AudioSource.PlayOneShot(m_pointSound);
                                    SetHUDSprite(PHOTO_HUD.POINT);
                                }
                            }
                        }
                        else
                        {
                            m_playedPointSound = false;
                            SetHUDSprite(PHOTO_HUD.POINTING);
                        }
                    }
                    else
                    {
                        SetHUDSprite(PHOTO_HUD.POINTING);
                        hitObject = null;
                        m_playedPointSound = false;
                    }
                }
                else
                {
                    if (Snap_InputManager.Instance && Snap_InputManager.Instance.PointDownInput())
                    {
                        m_playerScript.StopFlutePlay();
                        m_photoIcon.SetActive(true);
                        m_normalIconGroup.SetActive(false);
                        m_isZoomed = true;
                        m_zoomingOut = false;
                        m_zooming = true;
                        m_AudioSource.PlayOneShot(m_cameraInSound);
                        m_pointing = true;                      
                        SetHUDSprite(PHOTO_HUD.POINTING);
                        return;
                    }
                    else
                    {
                        if (m_canZoom)
                        {
                            if (m_zoomingOut)
                            {
                                CameraZoomOut();
                            }
                        }

                        SetHUDSprite(PHOTO_HUD.NORMAL);
                    }
                }
            }
        }

        /// <summary>
        /// Paints the HUD pointer
        /// </summary>
        void OnGUI()
        {
            if (m_hudVisible)
            {
                GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, m_sizeX, m_sizeY), m_tCross);
            }
        }

        /// <summary>
        /// When quiting in-game, we delete temp data
        /// </summary>
        private void OnApplicationQuit()
        {
            DeletePics();
        }

        #endregion

        #region Texts
        /// <summary>
        /// We parse static texts based on the current language
        /// </summary>
        public void ParseTexts()
        {
            m_numPhotoText.text = m_numOfPhotos.ToString();
            m_TrainerName.text = Snap_SaveManager.Instance.GetTrainerName();
            m_courseNameSelection.text = Snap_DialogManager.Instance.ParseXML.getTextInfo(m_stage);
            m_IChooseThisText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("ichoosethis");
            m_photoBy.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("photoby");
            m_courseSelection.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("course");
            m_toProfOakButton.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("goToOak");
            m_oaksMarkButton.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("oakMark");
            m_albumMarkButton.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("albumMark");
            m_newBigIcon.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("newIcon");

            // CAMERA CHECK
            m_cameraCheckText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("cameraCheck");
            m_cameraCheckCourseText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("course");
            m_cameraCheckNameText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo(m_stage);

            m_BackAllPhotos.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("backMayusc");

            m_outText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("out");
            m_outOfFilmText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("outOfFilm");

            m_instructionsSelectionText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("instSelection");

            m_confirmYesText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("yes");
            m_confirmNoText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("no");

            m_newInfoDiscover.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("newIcon");

            m_profCheckTitle.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("OakCheckTitle");
            m_thisTimeText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("thisTimeTxt");
            m_oldPicText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("oldPicTxt");

            m_newEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("newIcon");

            m_sizeEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("sizeEvaluation");
            m_poseEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("poseEvaluation");
            m_techEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("techEvaluation");
            m_sameBonusEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("samePkmnBonus");
            m_specialBonusEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("specialBonus");
            m_totalPtsEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("totalPts");
            m_previewPhotoEvaluationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("previousEvTxt");

            SetOakEvaluationText();
        }

        /// <summary>
        /// Set localization language so fungus changes the language of the texts
        /// </summary>
        public void SetOakEvaluationText()
        {
            string language = Snap_DialogManager.Instance.ParseXML.GetLang();
            m_localization.SetActiveLanguage(language, true);
        }

        #endregion

        #region Camera Zoom
        /// <summary>
        /// Camera zoom in effect (Based on field of view)
        /// </summary>
        public void CameraZoomIn()
        {
            m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, m_zoomFov, Time.deltaTime * zoomSmooth);

            if (m_Camera.fieldOfView - m_zoomFov <= m_zoomTolerance)
            {
                m_reachedLimit = true;
                m_zooming = false;
            }
        }
        /// <summary>
        /// Camera zoom out effect (Based on field of view)
        /// </summary>
        void CameraZoomOut()
        {
            m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, m_normalFov, Time.deltaTime * zoomSmooth);
            m_reachedLimit = false;

            if (m_normalFov - m_Camera.fieldOfView <= m_zoomTolerance)
            {
                m_zoomingOut = false;
            }
        }

        #endregion

        #region EndGame
        /// <summary>
        /// Wait some time after ending for playing a sound :D
        /// </summary>
        /// <returns></returns>
        IEnumerator EndGameCoroutine()
        {
            float timer = 0f;

            while (timer < m_waitedTimeToEvaluate)
            {
                timer += Time.deltaTime;
                yield return null;
            }

            yield return new WaitForSeconds(1f);

            ShowEvaluation();
        }

        /// <summary>
        /// Coroutine that allows the count of the pics (playing a sound when doing so)
        /// </summary>
        /// <returns></returns>
        IEnumerator CountPhotos()
        {
            int photos = m_maxNumOfPhotos-m_numOfPhotos;
            int counter = -1;
            string txt = Snap_DialogManager.Instance.ParseXML.getTextInfo("msgBoxCountingPhotos");

            while (counter < photos)
            {
                m_AudioSource.PlayOneShot(m_evaluationSFX);
                counter++;
                m_msgBoxText.text = counter + txt;
                yield return new WaitForSeconds(.2f);

            }

            yield return new WaitForSeconds(1f);
            BackToAllPhotos();
        }

        /// <summary>
        /// This method is called from the Oak Face Button. 
        /// It saves in a Dictionary the data of the current pic (Selected)
        /// If a pre-selected photo is selected again, the pic is deselected
        /// </summary>
        public void SelectForOaksEvaluation()
        {
            string _key = m_currentPhoto.PokemonKey;
            string _name = m_currentPhoto.PokemonName;

            m_previewSelectionGO.SetActive(false);

            if (m_oakPhotos.ContainsKey(_key))
            {
                //Substitute previous
                if (m_oakPhotos[_key] != m_currentPhoto)
                {
                    m_oakPhotos[_key] = m_currentPhoto;
                    m_previousTexture.texture = m_currentPhoto.PhotoTexture.texture;
                    m_previousOakGO.SetActive(true);

                    if (m_oakPhotos[_key].NewPokemon)
                        m_previousNewGO.SetActive(true);

                    if (m_showSelectedPics)
                    {
                        int c = m_selectedKeys.IndexOf(_key);
                        GameObject g = m_selectedPicsGO[c];
                        if (g)
                        {
                            m_selectedPicsGO.Remove(g);
                            Destroy(g);
                        }
                        m_selectedPicsGO.Insert(c, Instantiate(m_oakPhotos[_key].gameObject, m_panelSelectedPicsGO.transform));
                    }
                }
                else
                {
                    if (m_showSelectedPics)
                    {
                        int c = m_selectedKeys.IndexOf(_key);
                        GameObject g = m_selectedPicsGO[c];
                        if (g)
                        {
                            m_selectedPicsGO.Remove(g);
                            Destroy(g);
                        }
                    }

                    m_selectedKeys.Remove(_key);
                    m_oakPhotos.Remove(_key);
                    m_chooseGroup.SetActive(false);
                    m_previousOakGO.SetActive(false);
                    m_previousNewGO.SetActive(false);

                    if (!m_currentPhoto.NewPokemon)
                        m_previewSelectionGO.SetActive(true);
                }

                if (!m_albumPhotos.ContainsKey(_key))
                {
                    m_previousAlbumGO.SetActive(false);
                }
            }
            else
            {
                m_selectedKeys.Add(_key);
                m_oakPhotos.Add(_key, m_currentPhoto);
                m_chooseGroup.SetActive(true);

                if (m_showSelectedPics)
                {
                    m_selectedPicsGO.Add(Instantiate(m_currentPhoto.gameObject, m_panelSelectedPicsGO.transform));
                }

                if (m_currentPhoto.NewPokemon)
                    m_previousNewGO.SetActive(true);

                m_previousOakGO.SetActive(true);
                m_previousTexture.texture = m_currentPhoto.PhotoTexture.texture;

                if (m_albumPhotos.ContainsKey(_key))
                {
                    m_previousAlbumGO.SetActive(true);
                }
            }
        }

        /// <summary>
        /// We select for album so we know which photo can be shown l8r in Album Screen
        /// </summary>
        public void SelectForAlbum()
        {
            string _name = m_currentPhoto.PokemonName;

            if (m_albumPhotos.ContainsKey(_name))
            {
                //Substitute previous
                if (m_albumPhotos[_name] != m_currentPhoto)
                {
                    m_albumPhotos[_name] = m_currentPhoto;
                }
                else
                {
                    m_albumPhotos.Remove(_name);
                    m_previousAlbumGO.SetActive(false);                    
                }
            }
            else
            {
                m_albumPhotos.Add(_name, m_currentPhoto);
                m_previousAlbumGO.SetActive(true);
            }
        }

        /// <summary>
        /// This is called from Confirm Panel in Prof. Oak Check. 
        /// </summary>
        /// <param name="option">Yes = Can go out with/out pics to oak or exit, no = back again</param>
        public void ConfirmExitPhotoSelection(bool option)
        {
            if (option)
            {
                if (m_oakPhotos.Count == 0)
                {
                    Snap_MenuManager.Instance.GoBackToOakScene();
                }
                else
                {
                    InitCrossExamination();
                }
            }
            else
            {
                m_confirmSelection.SetActive(false);
                m_panelAllPhotos.SetActive(true);
                m_groupSelectedPicsGO.SetActive(false);
            }
        }

        /// <summary>
        /// This method is called from Go To Oak Button 
        /// </summary>
        public void GoBackToOak()
        {
            m_confirmSelection.SetActive(true);

            if (m_oakPhotos.Count == 0)
            {              
                m_confirmMsgText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("confirmOutPhotoSelection");
            }            
            else
            {
                m_confirmMsgText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("confirmOutWithPhotoSelection");

                m_panelSelectPhotos.SetActive(false);
                m_panelAllPhotos.SetActive(false);
                m_groupSelectedPicsGO.SetActive(true);
            }
        }

        /// <summary>
        /// We set every New Pokémon to the album so it knows it's discovered!
        /// </summary>
        public void InitCrossExamination()
        {
            foreach (Snap_Photo photo in m_oakPhotos.Values)
            {
                if (photo.NewPokemon)
                {
                    Snap_AlbumManager.Instance.SetNewPokemon(photo);
                }
            }

            m_evaluationGO.SetActive(true);
            m_confirmSelection.SetActive(false);
        }

        /// <summary>
        /// LET'S PROF CHECK STUFF!
        /// </summary>
        public void ShowEvaluation()
        {
            if (!m_tutorialCompleted)
            {
                m_tutorialCompleted = true;
                Snap_SaveManager.Instance.GetGameData.m_tutorialComplete = m_tutorialCompleted;
                Snap_SaveManager.Instance.SetTutorialFungus();
            }

            m_bgmSource.loop = true;
            m_bgmSource.clip = m_evaluationTheme;
            m_bgmSource.Play();

            m_cameraCheckGroup.SetActive(true);
            StartCoroutine(CountPhotos());
        }

        /// <summary>
        /// Show the selection panel showing (bigger size) the current picture
        /// </summary>
        /// <param name="photo"></param>
        public void ShowSelectionPhotoPanel(Snap_Photo photo)
        {
            m_currentPhoto = photo;
            m_panelSelectPhotos.SetActive(true);
            m_selectedTexture.texture = photo.PhotoTexture.texture;

            m_confirmSelection.SetActive(false);

            m_oakMarkBtn.gameObject.SetActive(true);
            m_albumMarkBtn.gameObject.SetActive(true);

            string _name = photo.PokemonName;
            bool isSignal = photo.IsSignal;

            if (_name == "Null")
            {
                _name = "MISSINGPKMN";
                m_oakMarkBtn.gameObject.SetActive(false);
                m_albumMarkBtn.gameObject.SetActive(false);
            }
            else // if can be analyzed
            {
                if (!m_selectedKeys.Contains(_name))
                {
                    if (photo.NewPokemon)
                    {
                        m_previewSelectionGO.SetActive(false);
                    }
                    else
                    {
                        LoadOldPreview(_name);
                    }
                }
            }

            m_pokemonNameSelection.text = _name;
            m_newBigIconGO.SetActive(photo.NewPokemon);

            if (m_oakPhotos.ContainsKey(_name))
            {
                m_chooseGroup.SetActive(true);
                m_previousOakGO.SetActive(true);

                if (m_oakPhotos[_name].NewPokemon)
                {
                    m_previousNewGO.SetActive(true);
                }

                m_previousTexture.texture = m_oakPhotos[_name].PhotoTexture.texture;
            }

            m_msgBoxText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("msgBoxSelectingPhoto");
        }

        /// <summary>
        /// Method that goes back to all photo section (where you select them)
        /// </summary>
        public void BackToAllPhotos()
        {
            m_currentPhoto = null;
            m_chooseGroup.SetActive(false);
            m_previewSelectionGO.SetActive(false);
            m_panelSelectPhotos.SetActive(false);
            m_playerScript.MouseLook.SetCursorLock(false);
            m_panelAllPhotos.SetActive(true);
            m_previousOakGO.SetActive(false);
            m_previousNewGO.SetActive(false);
            m_msgBoxText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("msgBoxAllPhotos");
        }

        #endregion

        #region Screenshots
        /// <summary>
        /// Method for taking pictures (This calls to render to texture method)
        /// </summary>
        public void TakePicture()
        {
            Texture2D texture;

            if (!Directory.Exists(m_photoFolder))
            {
                Directory.CreateDirectory(m_photoFolder);
            }

            if (m_usingScreenCapture)
            {
                ScreenCapture.CaptureScreenshot(m_photoFolder + "/" + m_stage + m_numOfPhotos + ".png");
                m_numOfPhotos--;
                m_numPhotoText.text = m_numOfPhotos.ToString();
            }
            else
            {
                if (m_usePostProcessing && m_useMotionBlur)
                {
                    m_PostProProfile.motionBlur.enabled = false;
                    texture = TakeSnapshot(m_photoFolder + "/" + m_stage + m_numOfPhotos + ".png");
                    m_numOfPhotos--;
                    m_numPhotoText.text = m_numOfPhotos.ToString();
                    m_PostProProfile.motionBlur.enabled = true;
                }
                else
                {
                    texture = TakeSnapshot(m_photoFolder + "/" + m_stage + m_numOfPhotos + ".png");
                    m_numOfPhotos--;
                    m_numPhotoText.text = m_numOfPhotos.ToString();
                }

                SnapEvaluationData(texture);
            }
        }

        /// <summary>
        /// Method that checks if the pokémon is in the middle
        /// If a pokémon touches the center AND any of the other rays,
        /// it's not considered "Centered"
        /// </summary>
        bool CheckPokemonInMiddleOfTheFrame()
        {
            // We will use eight extra rays

            // LEFT TOP CORNER
            Vector3 pos = new Vector3(m_Camera.transform.position.x - Screen.width / 2, m_Camera.transform.position.y + Screen.height / 2, m_Camera.transform.position.z);
            Ray _ray = new Ray(pos, m_Camera.transform.forward);
            RaycastHit _hit;

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            // TOP MIDDLE
            pos = new Vector3(m_Camera.transform.position.x, m_Camera.transform.position.y + Screen.height / 2, m_Camera.transform.position.z);
            _ray = new Ray(pos, m_Camera.transform.forward);

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            // CORNER TOP RIGHT
            pos = new Vector3(m_Camera.transform.position.x + Screen.width/2, m_Camera.transform.position.y + Screen.height / 2, m_Camera.transform.position.z);
            _ray = new Ray(pos, m_Camera.transform.forward);

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            // MIDDLE LEFT SIDE
            pos = new Vector3(m_Camera.transform.position.x - Screen.width / 2, m_Camera.transform.position.y, m_Camera.transform.position.z);
            _ray = new Ray(pos, m_Camera.transform.forward);

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            // MIDDLE RIGHT SIDE
            pos = new Vector3(m_Camera.transform.position.x + Screen.width / 2, m_Camera.transform.position.y, m_Camera.transform.position.z);
            _ray = new Ray(pos, m_Camera.transform.forward);

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            // LEFT BOTTOM CORNER
            pos = new Vector3(m_Camera.transform.position.x - Screen.width / 2, m_Camera.transform.position.y-Screen.height/2, m_Camera.transform.position.z);
            _ray = new Ray(pos, m_Camera.transform.forward);

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            // RIGHT BOTTOM CORNER
            pos = new Vector3(m_Camera.transform.position.x + Screen.width / 2, m_Camera.transform.position.y - Screen.height / 2, m_Camera.transform.position.z);
            _ray = new Ray(pos, m_Camera.transform.forward);

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            // BOTTOM MIDDLE
            pos = new Vector3(m_Camera.transform.position.x, m_Camera.transform.position.y - Screen.height / 2, m_Camera.transform.position.z);
            _ray = new Ray(pos, m_Camera.transform.forward);

            if (Physics.Raycast(_ray, out _hit, m_detectDistance, m_layerMaskInteract.value))
            {
                if (hitObject == _hit.transform.gameObject)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Method that checks if more pokémon than the main one are being snapped!
        /// Need to find a better way to check this
        /// </summary>
        /// <param name="_key"></param>
        void CheckMorePokemon(string _key = "TEST")
        {
            // We check every pkmn in the course. 
            // TODO: FIND A BETTER WAY TO DO THIS
            foreach (Snap_Pokemon pokemon in m_pokemonList)
            {
                m_moreThanOne = false;
                m_samePokemon = false;

                if (pokemon.IsRendered)
                {
                    if (pokemon.gameObject != hitObject)
                    {
                        m_moreThanOne = true;
                        if (pokemon.PokemonKey == _key)
                        {
                            m_samePokemon = true;
                        }
                        // if we find one more pkmn, we can return
                        return;
                    }
                }
            }
                //m_moreThanOne = false;
                //m_samePokemon = false;
        }

        /// <summary>
        /// When taking a Picture, this method creates all the needed data 
        /// for evaluation values and stores it in a List
        /// </summary>
        /// <param name="texture"></param>
        void SnapEvaluationData(Texture2D texture)
        {
            GameObject _base = Instantiate(m_photoBase, m_evaluatePhotoPanel.transform);
            Snap_Photo temp = _base.GetComponent<Snap_Photo>();

            RawImage sp = _base.GetComponent<RawImage>();
            sp.texture = texture;

            if (hitObject && hitObject.tag == "Pokemon")
            {
                Snap_Pokemon data = hitObject.GetComponent<Snap_Pokemon>();
                m_isSignal = data.IsSignal;
                bool canPhoto = data.CanBePhotographied;

                if (m_isSignal && !m_canPhotoSignal)
                    canPhoto = false;

                if (canPhoto)
                    m_pokemonKey = data.PokemonKey;
                else
                    m_pokemonKey = "Null";

                m_pokemonName = data.PokemonName;
                m_distanceToPokemon = (hitObject.transform.position - this.transform.position).magnitude;
                m_poseMultiplier = data.PoseMultiplier;

                // x2 = centered or x1 = not centered
                m_distanceToCenter = (CheckPokemonInMiddleOfTheFrame()) ? 2f : 1f;
                CheckMorePokemon(m_pokemonKey);

                //m_moreThanOne = false;
                //m_samePokemon = false;

                m_bonusPts = data.BonusPoints;

                m_specialBonus = data.SpecialBonus;
                m_newPokemon = !Snap_AlbumManager.Instance.IsPokemonInAlbum(m_pokemonKey);
                m_discoveryName.text = m_pokemonName;

                if (m_newPokemon)
                {
                    m_discoveryAnimator.SetTrigger(m_discoveryNewTrigger);
                }
                else
                {
                    m_discoveryAnimator.SetTrigger(m_discoveryTrigger);
                }
                
                temp.SetPhotoData(m_pokemonKey, m_pokemonName, m_distanceToPokemon, m_poseMultiplier, m_distanceToCenter, m_moreThanOne, m_samePokemon, m_specialBonus, m_newPokemon, (m_numOfPhotos+1), m_isSignal, m_bonusPts);
            }
            else
            {
                temp.SetPhotoDataNull();              
            }

            m_photoList.Add(temp);
        }

        /// <summary>
        /// When pausing we need to Unzoom and set the hud visibility to false
        /// </summary>
        public void SetPointingUnpause()
        {
            if (m_canZoom)
            {
                m_photoIcon.SetActive(false);
                m_normalIconGroup.SetActive(true);
                m_isZoomed = false;
                m_zooming = false;
                m_zoomingOut = true;
            }

            m_AudioSource.PlayOneShot(m_cameraOutSound);
            m_pointing = false;
        }

        /// <summary>
        /// We use Render to texture to make this possible. With a coroutine we can avoid having "waiting bugs".
        /// </summary>
        /// <returns></returns>
        IEnumerator TakeScreenShotBufferPNG()
        {
            m_hudVisible = false;
            m_blackScreen.SetActive(true);
            m_processingScreenshot = true;
            float timer = 0;

            Snap_EventManager.TriggerEvent(m_snapShotTriggerEvent);

            m_photoIconAnimator.SetTrigger(m_snapTrigger);
            m_AudioSource.PlayOneShot(m_snapShotSound);

            TakePicture();

            while (timer < m_timeBetweenPics)
            {
                timer += Time.deltaTime;
                yield return null;
            }

            m_processingScreenshot = false;
            m_blackScreen.SetActive(false);
            m_hudVisible = true;

            if (m_numOfPhotos <= m_minNumOfPhotos)
            {               
                EndNoMorePics();
            }
        }

        public Texture2D TakeSnapshot(string _path)
        {
            // capture the virtuCam and save it as a square PNG.

            int sqr = 512;
            int width = Screen.width;
            int height = Screen.height;

            //m_Camera.aspect = 1.0f;
            // recall that the height is now the "actual" size from now on

            RenderTexture tempRT = new RenderTexture(width, height, 24);
            // the 24 can be 0,16,24, formats like
            // RenderTextureFormat.Default, ARGB32 etc.

            m_Camera.targetTexture = tempRT;
            m_Camera.Render();

            RenderTexture.active = tempRT;
            Texture2D virtualPhoto = new Texture2D(width, height, TextureFormat.RGB24, false);
            // false, meaning no need for mipmaps
            virtualPhoto.ReadPixels(new Rect(0, 0, width, height), 0, 0);
            virtualPhoto.Apply();

            Texture2D returnPhoto = new Texture2D(sqr, sqr, TextureFormat.RGB24, false);
            returnPhoto.ReadPixels(new Rect(width/2-sqr/2, height/2-sqr/2, sqr, sqr), 0, 0);
            returnPhoto.Apply();

            RenderTexture.active = null; //can help avoid errors 
            m_Camera.targetTexture = null;

            Destroy(tempRT);

            SavePNGFromTexture(virtualPhoto, _path);

            return returnPhoto;
        }

        /// <summary>
        /// Method that saves a png based on png and render to texture
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="path"></param>
        public void SavePNGFromTexture(Texture2D texture, string path)
        {
            byte[] bytes;
            bytes = texture.EncodeToPNG();

            File.WriteAllBytes(path, bytes);
        }

        /// <summary>
        /// We need to create the photo folder if it's not in the game data
        /// </summary>
        void CreatePhotoFolder()
        {
            m_photoFolder = Application.dataPath + "/Resources/" + m_photoFolderName;
            if (!Directory.Exists(m_photoFolder))
            {
                Directory.CreateDirectory(m_photoFolder);
            }
        }
        /// <summary>
        /// We need to create the album folder if it's not in the game data
        /// </summary>
        void CreateAlbumFolder()
        {
            m_albumFolder = Application.dataPath + "/Resources/" + m_albumFolderName;
            if (!Directory.Exists(m_albumFolder))
            {
                Directory.CreateDirectory(m_albumFolder);

                if (!Directory.Exists(m_albumFolder + "/" + m_stage))
                {
                    Directory.CreateDirectory(m_albumFolder + "/" + m_stage);
                }
            }
        }
        #endregion

        #region HUD
        /// <summary>
        /// This method changes the size and the icon of the HUD in the middle of the screen (The pointer)
        /// </summary>
        /// <param name="_type"></param>
        void SetHUDSprite(PHOTO_HUD _type)
        {
            if (m_hudType != _type)
            {
                m_hudType = _type;

                switch (_type)
                {
                    case PHOTO_HUD.NORMAL:
                        m_tCross = icons[0];
                        m_sizeX = m_normalHUDSize;
                        m_sizeY = m_normalHUDSize;
                        break;
                    case PHOTO_HUD.POINT:
                        m_tCross = icons[1];
                        m_sizeX = m_pointHUDSize;
                        m_sizeY = m_pointHUDSize;
                        break;
                    case PHOTO_HUD.POINTING:
                        m_tCross = icons[2];
                        m_sizeX = m_pointHUDSize;
                        m_sizeY = m_pointHUDSize;
                        break;
                }
            }
        }

        #endregion

        #region More Menu
        /// <summary>
        /// Continue game. (UNPAUSE Pressed from Resume Button)
        /// </summary>
        public void ContinueGame()
        {
            m_hudVisible = true;
            SetPointingUnpause();
            m_playerScript.UnPause();
        }

        /// <summary>
        /// End game when finishing the course (Final gate)
        /// </summary>
        public void EndCourse()
        {

            m_hudVisible = false;
            Snap_MenuManager.m_endGame = true;
            m_playerScript.PauseController();
            StartCoroutine(EndGameCoroutine());
        }

        /// <summary>
        /// Having 0 pics, call this method. It shows the blue screen with a music and goes to photo counter
        /// </summary>
        public void EndNoMorePics()
        {

            PlayerScript.Stop();
            m_bgmSource.loop = false;
            m_bgmSource.clip = m_endOfFilmSound;
            m_bgmSource.Play();
            m_endPanelNoPics.SetActive(true);
            EndCourse();
        }

        /// <summary>
        /// Set initial data for fungus so it can count stuff
        /// </summary>
        public void SetFungusData()
        {
            m_flowChart.SetIntegerVariable("numberOfPics", m_selectedKeys.Count);
            m_flowChart.SetIntegerVariable("counter", 0);
        }

        public void LoadOldPreview(string _photoKey)
        {
            m_previewSelectionGO.SetActive(true);
            string texPath = m_albumFolderName + "/" + _photoKey;
            Texture2D tx = Resources.Load<Texture2D>(texPath) as Texture2D;

            if (tx)
            {
                int sqr = 512;
                /*
                Color[] c = tx.GetPixels(tx.width - sqr/2, tx.height - sqr / 2, sqr, sqr);
                Texture2D tex = new Texture2D(sqr, sqr);
                tex.SetPixels(c);
                tex.Apply();

                m_oldImageTexture.texture = tex;
                */

                m_previewSelectionImg.texture = tx;
            }
        }

        public void LoadFungusData()
        {
            int counter = m_flowChart.GetIntegerVariable("counter");
            string _photoKey = m_selectedKeys[counter];
            
            Snap_Photo data = m_oakPhotos[_photoKey];
            bool _isNew = data.NewPokemon;
            string _pokemonName = data.PokemonName;
            float _totalPts = data.TotalPoints;
            m_newImageTexture.texture = data.PhotoTexture.texture;

            m_flowChart.SetBooleanVariable("isNew", _isNew);
            m_flowChart.SetStringVariable("pokemonName", data.PokemonName);
            m_flowChart.SetFloatVariable("totalPts", _totalPts);
            m_flowChart.SetFloatVariable("sizePts", data.SizePoints);
            m_flowChart.SetFloatVariable("posePts", data.PosePoints);
            m_flowChart.SetFloatVariable("techPts", data.TechPoints);
            m_flowChart.SetFloatVariable("otherPts", data.OtherPoints);
            m_flowChart.SetFloatVariable("bonusPts", data.BonusPoints);
            m_flowChart.SetStringVariable("bonusPhrase", data.BonusPhrase);

            string path = m_photoFolder + "/" + m_stage + data.PhotoNumber + ".png";
            string dest = m_albumFolder + "/" + _photoKey + ".png";

            if (_isNew)
            {
                if (File.Exists(dest))
                {
                    File.Delete(dest);
                }

                File.Move(path, dest);

                Snap_AlbumManager.Instance.AlbumDictionary[_photoKey].m_points = _totalPts;
            }
            else
            {              
                Snap_AlbumPhoto old = Snap_AlbumManager.Instance.AlbumDictionary[_photoKey];
                float p = old.m_points;

                Debug.Log("The previous pic of " + _photoKey + " has " +p);

                m_flowChart.SetFloatVariable("oldPicPts", p);
                string texPath = m_albumFolderName + "/" + _photoKey;
                Texture2D tx = Resources.Load<Texture2D>(texPath) as Texture2D;

                if (tx)
                {
                    int sqr = 512;
                    /*
                    Color[] c = tx.GetPixels(tx.width - sqr/2, tx.height - sqr / 2, sqr, sqr);
                    Texture2D tex = new Texture2D(sqr, sqr);
                    tex.SetPixels(c);
                    tex.Apply();

                    m_oldImageTexture.texture = tex;
                    */

                    m_oldImageTexture.texture = tx;
                }
                else
                {
                    Debug.Log("Not Found " + texPath);
                }

                if (_totalPts >= p)
                {
                    Debug.Log("New one has more points");
                    Snap_AlbumManager.Instance.AlbumDictionary[_photoKey].m_points = _totalPts;
                    if (File.Exists(dest))
                    {
                        Debug.Log("Deleting previous");
                        File.Delete(dest);
                    }
                    File.Move(path, dest);
                }
                else
                {
                    Debug.Log("Old one has more points");
                }
            }
        }

        public void SaveNewPic()
        {

        }

        public void GoOut()
        {
            DeletePics();
            Snap_MenuManager.Instance.GoBackToOakScene();
        }

        public void DeletePics()
        {
            string[] tempPics = Directory.GetFiles(m_photoFolder);

            foreach (string file in tempPics)
            {
                File.Delete(file);
            }
        }

        public void OnButtonEnter()
        {
            m_buttonSource.PlayOneShot(m_buttonEnter);
        }

        public void OnButtonPress()
        {
            m_buttonSource.PlayOneShot(m_buttonConfirm);
        }
        #endregion
    }

}