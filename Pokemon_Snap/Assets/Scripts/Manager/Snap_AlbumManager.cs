﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Manager that controls the data for Album (Saved pics... and stuff)
    /// </summary>
    public class Snap_AlbumManager : MonoBehaviour
    {
        #region Singleton Pattern
        /// <summary>
        /// Singleton Instance for having just one
        /// </summary>
        private static Snap_AlbumManager instance;
        public static Snap_AlbumManager Instance { get { return instance; } }
        #endregion

        #region Variables
        /// <summary>
        /// If we want to destroy this instance between scenes
        /// </summary>
        [SerializeField] private bool m_destroyOnLoad = false;
        /// <summary>
        /// Original JSON. When starting a new game, data is read from this file
        /// </summary>
        [SerializeField] private string m_albumOriginalJSON = "GameData/Album/AlbumData";
        /// <summary>
        /// Modified JSON by playing. It stores some game data refered to discovered Pokémon
        /// </summary>
        [SerializeField] private string m_albumJSON = "GameData/GameData/AlbumData";
        /// <summary>
        /// Album folder (where the pics are saved)
        /// </summary>
        [SerializeField] private string m_albumFolder = "Resources/AlbumPics/";

        /// <summary>
        /// Photo Dictionary
        /// </summary>
        private Dictionary<string, Snap_AlbumPhoto> m_vAlbumPokemon;
        public Dictionary<string, Snap_AlbumPhoto> AlbumDictionary { get { return m_vAlbumPokemon; } set { m_vAlbumPokemon = value; } }

        public List<Snap_AlbumPhoto> m_album;

        #endregion

        #region MonoBehaviour
        private void Awake()
        {
            if (instance == null)
            {            
                instance = this;

                if (!m_destroyOnLoad)
                    DontDestroyOnLoad(this);

                m_vAlbumPokemon = new Dictionary<string, Snap_AlbumPhoto>();
                m_album = new List<Snap_AlbumPhoto>();

            }
            else
            {
                Destroy(this.gameObject);
            }           
        }

        private void Start()
        {
            m_albumFolder = Application.dataPath + "/" + m_albumFolder;

            if (!Directory.Exists(m_albumFolder))
            {
                Directory.CreateDirectory(m_albumFolder);
            }
        }
        #endregion

        #region JSON DATA (LOAD, SAVE...)
        /// <summary>
        /// Set data about Pokémon in the JSON
        /// </summary>
        /// <param name="photo"></param>
        public void SetNewPokemon(Snap_Photo photo)
        { 

            string _key = photo.PokemonKey;
            float pts = photo.TotalPoints;

            // THIS SHOULDNT HAPPEN
            if (!m_vAlbumPokemon.ContainsKey(_key))
            {
                Debug.Log("ADDING A NEW PIC WITH KEY " + _key + ". PLEASE, CHECK THIS AS THIS SHOULD NEVER HAPPEN");
                Snap_AlbumPhoto pic = new Snap_AlbumPhoto(photo.PhotoNumber, photo.PokemonName, true, pts, photo.IsSignal);
                m_vAlbumPokemon.Add(_key, pic);
            }
                
            m_vAlbumPokemon[_key].m_discovered = true;

#if UNITY_EDITOR
            foreach (Snap_AlbumPhoto p in m_album)
            {
                if (p.m_key == _key)
                {
                    p.m_discovered = true;
                    break;
                }
            }
#endif
        }

        /// <summary>
        /// Save album data to JSON
        /// </summary>
        public void SaveData()
        {
            Debug.Log("Saving Album Data");

            string _newPath = Application.dataPath + "/Resources/" + m_albumJSON + ".json";
            JSONNode info = JSONNode.LoadFromFile(_newPath);

            string key;

            for (int i = 0; i < info["Album"].Count; i++)
            {
                key = info["Album"][i]["Key"];
                info["Album"][i]["Discovered"] = m_vAlbumPokemon[key].m_discovered.ToString();
                info["Album"][i]["Points"] = m_vAlbumPokemon[key].m_points.ToString();
            }

            info.SaveToFile(_newPath);
        }

        /// <summary>
        /// Load Data from modified JSON
        /// </summary>
        public void LoadAlbumData()
        {
            string _newPath = Application.dataPath + "/Resources/" + m_albumJSON + ".json";
            JSONNode info = JSONNode.LoadFromFile(_newPath);

            for (int i = 0; i < info["Album"].Count; i++)
            {
                Snap_AlbumPhoto album_photo = new Snap_AlbumPhoto(info["Album"][i]);
                m_vAlbumPokemon.Add(album_photo.m_key, album_photo);
#if UNITY_EDITOR
                m_album.Add(album_photo);
#endif
            }
        }

        /// <summary>
        /// Load Data from original JSON (New Game)
        /// </summary>
        public void LoadOriginalAlbumData()
        {
            string _originalPath = Application.dataPath + "/Resources/" + m_albumOriginalJSON + ".json";
            string _newPath = Application.dataPath + "/Resources/" + m_albumJSON + ".json";

            string json = Resources.Load(m_albumOriginalJSON).ToString();

            JSONNode info = JSONNode.Parse(json);

            for (int i = 0; i < info["Album"].Count; i++)
            {
                Snap_AlbumPhoto album_photo = new Snap_AlbumPhoto(info["Album"][i]);
                m_vAlbumPokemon.Add(album_photo.m_key, album_photo);
#if UNITY_EDITOR
                m_album.Add(album_photo);
#endif
            }

            info.SaveToFile(_newPath);

            DeleteEveryPic();
        }

        /// <summary>
        /// Check if the Pokémon is already stored in the album
        /// </summary>
        /// <param name="_pokemonName"></param>
        /// <returns></returns>
        public bool IsPokemonInAlbum(string _pokemonKey)
        {
#if UNITY_EDITOR
            foreach (Snap_AlbumPhoto p in m_album)
            {
                if (p.m_key == _pokemonKey)
                {
                    return p.m_discovered;
                }
            }

            return false;
#else
            return m_vAlbumPokemon[_pokemonKey].m_discovered;
#endif
        }

        /// <summary>
        /// Delete every pic in album folder
        /// </summary>
        public void DeleteEveryPic()
        {
            //Debug.Log("Deleting from:" + m_albumFolder);
            string[] tempPics = Directory.GetFiles(m_albumFolder);

            foreach (string file in tempPics)
            {
                File.Delete(file);
            }
        }
#endregion
    }
}
