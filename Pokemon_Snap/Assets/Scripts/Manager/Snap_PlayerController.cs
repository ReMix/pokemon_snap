﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace PokemonSnapUnity
{
    /// <summary>
    /// This class controls the main mechanics for the player such as run, throw pester/apple or play the flute
    /// </summary>
    public class Snap_PlayerController : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent m_navmeshAgent;
        [SerializeField] private Transform[] m_waypoints;
        [SerializeField] private int m_destPoint = 0;
        [SerializeField] private float m_minimumDistance = 0.5f;
        [SerializeField] private bool m_usingRAIN = false;
        [SerializeField] private AudioSource m_runSource;
        [SerializeField] private AudioClip m_runSound;
        [SerializeField] private Animator m_zAnimator;
        [SerializeField] private RAIN.Core.AIRig m_memoryData;
        [SerializeField] private Snap_MouseLook m_MouseLook;
        [SerializeField] private Camera m_Camera;
        [SerializeField] private float m_movementSpeed = 1f;
        [SerializeField] private float m_runSpeed = 3f;
        [SerializeField] private bool m_running = false;
        [SerializeField] private bool m_paused = false;

        [Header("~ Unlocked Features ~"), Space(10)]
        [SerializeField] private Transform m_CameraTransform;
        [SerializeField] private Transform m_ThrowerTransform;
        [SerializeField] private GameObject m_runIcon;
        [SerializeField] private AudioSource m_throwSource;
        [SerializeField] private AudioClip m_throwSound;

        // Apple
        [SerializeField] private GameObject m_appleIcon;
        [SerializeField] private GameObject m_appleGO;
        [SerializeField] private Animator m_appleAnimator;
        [SerializeField] private string m_triggerUseapple = "use";
        [SerializeField] private float m_appleForce = 500f;
        [SerializeField] private float m_appleTimer = 0f;
        [SerializeField] private float m_delayBetweenApples = .5f;
        [SerializeField] private int m_applePoolAmount = 5;
        private List<GameObject> m_appleList;

        // Pester 
        [SerializeField] private GameObject m_pesterIcon;
        [SerializeField] private GameObject m_PesterBallGO;
        [SerializeField] private Animator m_pesterAnimator;
        [SerializeField] private string m_triggerUsePester = "use";
        [SerializeField] private float m_pesterForce = 500f;
        [SerializeField] private float m_pesterTimer = 0f;
        [SerializeField] private float m_delayBetweenPesters = .5f;
        [SerializeField] private int m_pesterBallPoolAmount = 5;
        private List <GameObject> m_pesterBallList;

        //Flute
        [SerializeField] private AudioSource m_bgmSource;
        [SerializeField] private GameObject m_fluteAIEntity;
        [SerializeField] private GameObject m_fluteIcon;
        [SerializeField] private Animator m_fluteAnimator;
        [SerializeField] private AudioSource m_fluteSource;
        [SerializeField] private AudioClip[] m_fluteClips;
        [SerializeField] private int m_currentClip = 0;
        [SerializeField] private string m_triggerPlayFlute = "play";
        [SerializeField] private string m_triggerIdleFlute = "idle";
        [SerializeField] private bool m_playingFlute = false;

        [SerializeField] private bool m_canUseRun = false;
        [SerializeField] private bool m_canUseApple = false;
        [SerializeField] private bool m_canUsePester = false;
        [SerializeField] private bool m_canUseFloute = false;

        public Snap_MouseLook MouseLook { get { return m_MouseLook; } }

        // Use this for initialization
        void Start()
        {
            if (!m_Camera)
                m_Camera = Camera.main;

            m_MouseLook.Init(transform, m_CameraTransform);
            m_running = false;
            m_memoryData.AI.IsActive = m_usingRAIN;

            if (m_usingRAIN)
                m_memoryData.AI.WorkingMemory.SetItem("moveSpeed", m_movementSpeed);
            else
                m_navmeshAgent.speed = m_movementSpeed;

            SetFeatures();
            m_runSource.Stop();

        }

        // Update is called once per frame
        void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Y))
            {
                m_canUsePester = true;
            }
#endif
            if (!Snap_MenuManager.m_endGame && !Snap_MenuManager.m_initialAnimation)
            {

                CheckPause();

                if (!m_paused)
                {
                    if (!m_navmeshAgent.pathPending && m_navmeshAgent.remainingDistance < m_minimumDistance)
                        GotoNextPoint();

                    RotateView();

                    if (m_canUseRun)
                    {
                        CheckRun();
                    }

                    if (!Snap_PhotoManager.Instance.Pointing)
                    {
                        if (m_canUseApple)
                        {
                            CheckThrowApple();
                        }

                        if (m_canUsePester)
                        {
                            CheckThrowPesterBall();
                        }

                        if (m_canUseFloute)
                        {
                            CheckFlute();
                        }
                    }
                }
            }
        }

        private void FixedUpdate()
        {
            m_MouseLook.UpdateCursorLock();
        }

        private void RotateView()
        {
            m_MouseLook.LookRotation(transform, m_Camera.transform);
        }

        private void CheckFlute()
        {
            if (!Snap_PhotoManager.Instance.ProcessingPhoto)
            {
                if (Snap_InputManager.Instance && Snap_InputManager.Instance.FluteInput())
                {
                    if (!m_playingFlute)
                    {
                        PlayFlute();
                    }
                    else
                    {
                        m_currentClip++;
                        if (m_currentClip >= m_fluteClips.Length)
                        {
                            m_currentClip = 0;
                        }
                    }

                    m_fluteSource.clip = m_fluteClips[m_currentClip];
                    m_fluteSource.Play();
                }
            }
        }

        private void CheckThrowApple()
        {
            if (!Snap_PhotoManager.Instance.ProcessingPhoto)
            {

                if (m_appleTimer > m_delayBetweenApples)
                {
                    if (Snap_InputManager.Instance && Snap_InputManager.Instance.ThrowAppleInput())
                    {
                        if (m_playingFlute)
                        {
                            StopFlutePlay();
                        }

                        m_throwSource.PlayOneShot(m_throwSound);
                        m_appleAnimator.SetTrigger(m_triggerUseapple);
                        ThrowApple();
                    }
                }
                else
                {
                    m_appleTimer += Time.deltaTime;
                }
            }
        }

        public void PlayFlute()
        {
            m_bgmSource.Stop();
            m_fluteAIEntity.SetActive(true);
            m_playingFlute = true;
            m_currentClip = 0;
            m_fluteAnimator.SetTrigger(m_triggerPlayFlute);
        }

        public void StopFlutePlay()
        {
            if (!m_playingFlute)
                return;

            m_playingFlute = false;
            m_currentClip = 0;
            m_bgmSource.Play();
            m_fluteAIEntity.SetActive(false);
            m_fluteSource.Stop();
            m_fluteAnimator.SetTrigger(m_triggerIdleFlute);
        }

        private void ThrowApple()
        {
            Debug.Log("Throwing apple");
            for (int i = 0; i < m_appleList.Count; i++)
            {
                if (!m_appleList[i].activeInHierarchy)
                {

                    m_appleList[i].transform.position = m_ThrowerTransform.position;
                    m_appleList[i].transform.rotation = m_ThrowerTransform.rotation;
                    m_appleList[i].SetActive(true);
                    m_appleList[i].GetComponent<Snap_Apple>().Throw(m_ThrowerTransform.forward * m_appleForce);
                    m_appleTimer = 0f;
                    break;
                }
            }
        }

        private void CheckThrowPesterBall()
        {
            if (!Snap_PhotoManager.Instance.ProcessingPhoto)
            {
               
                if (m_pesterTimer > m_delayBetweenPesters)
                {
                    if (Snap_InputManager.Instance && Snap_InputManager.Instance.PesterBallInput())
                    {
                        if (m_playingFlute)
                        {
                            StopFlutePlay();
                        }
                        m_pesterAnimator.SetTrigger(m_triggerUsePester);
                        m_throwSource.PlayOneShot(m_throwSound);
                        ThrowPesterBall();
                    }               
                }
                else
                {
                    m_pesterTimer += Time.deltaTime;
                }
            }
        }

        private void ThrowPesterBall()
        {
            for (int i = 0; i < m_pesterBallList.Count; i++)
            {
               if (!m_pesterBallList[i].activeInHierarchy)
               {
                    m_pesterBallList[i].transform.position = m_ThrowerTransform.position;
                    m_pesterBallList[i].transform.rotation = m_ThrowerTransform.rotation;
                    m_pesterBallList[i].SetActive(true);
                    m_pesterBallList[i].GetComponent<Snap_PesterBall>().Throw(m_ThrowerTransform.forward * m_pesterForce);
                    m_pesterTimer = 0f;
                    break;
                }
            }
        }

        private void CheckPause()
        {
            if (!Snap_PhotoManager.Instance.ProcessingPhoto)
            {
                if (Snap_InputManager.Instance && Snap_InputManager.Instance.PauseInput())
                {
                    Snap_MenuManager.Instance.MenuIngame();
                    m_paused = Snap_MenuManager.m_pauseGame;

                    if (m_usingRAIN)
                        m_memoryData.AI.WorkingMemory.SetItem("pausedGame", m_paused);
                    else
                        m_navmeshAgent.isStopped = true;

                    Snap_PhotoManager.Instance.HudVisible = !m_paused;

                    if (m_paused)
                    {
                        m_MouseLook.SetCursorLock(false);
                    }
                    else
                    {
                        m_MouseLook.SetCursorLock(true);
                        Snap_PhotoManager.Instance.SetPointingUnpause();
                    }
               
                }
            }

        }

        public void UnPause()
        {
            m_paused = false;

            if (m_usingRAIN)
                m_memoryData.AI.WorkingMemory.SetItem("pausedGame", false);
            else
                m_navmeshAgent.isStopped = false;

            m_MouseLook.SetCursorLock(true);
        }

        public void PauseController()
        {
            
            Snap_MenuManager.m_pauseGame = true;
            m_paused = Snap_MenuManager.m_pauseGame;

            if (m_usingRAIN)
                m_memoryData.AI.WorkingMemory.SetItem("pausedGame", m_paused);
        }

        #region Features()

        void CheckRun()
        {
            if (!m_running)
            {
                if (Snap_InputManager.Instance && Snap_InputManager.Instance.RunDownInput())
                {
                    m_runSource.Play();
                    m_running = true;
                    m_zAnimator.SetBool("running", true);
                    if (m_usingRAIN)
                        m_memoryData.AI.WorkingMemory.SetItem("moveSpeed", m_runSpeed);
                    else
                        m_navmeshAgent.speed = m_runSpeed;
                }
            }
            else
            {
                if (Snap_InputManager.Instance && Snap_InputManager.Instance.RunUpInput())
                {
                    m_runSource.Stop();
                    m_running = false;
                    m_zAnimator.SetBool("running", false);
                    if (m_usingRAIN)
                        m_memoryData.AI.WorkingMemory.SetItem("moveSpeed", m_movementSpeed);
                    else
                        m_navmeshAgent.speed = m_movementSpeed;
                }
            }
        }

        void SetFeatures()
        {
            Snap_SaveManager.GameData data = Snap_SaveManager.Instance.GetGameData;

            m_canUseRun = data.m_unlockedRun;

            if (m_canUseRun)
                m_runIcon.SetActive(true);

            m_canUseApple = data.m_unlockedApple;
            m_canUsePester = data.m_unlockedPesterBall;
            m_canUseFloute = data.m_unlockedFloute;

            if (m_canUseApple)
            {
                m_appleIcon.SetActive(true);
                // Activate Apple pool
                m_appleList = new List<GameObject>();
                for (int i = 0; i < m_applePoolAmount; i++)
                {
                    GameObject temp = Instantiate(m_appleGO);
                    temp.SetActive(false);
                    m_appleList.Add(temp);
                }
            }

            if (m_canUsePester)
            {
                m_pesterIcon.SetActive(true);
                // Activate Pesterball pool
                m_pesterBallList = new List<GameObject>();
                for (int i = 0; i < m_pesterBallPoolAmount; i++)
                {
                    GameObject temp = Instantiate(m_PesterBallGO);
                    temp.SetActive(false);
                    m_pesterBallList.Add(temp);
                }
            }

            if (m_canUseFloute)
            {
                m_fluteIcon.SetActive(true);
            }
        }

        #endregion

        #region Movement

        public void Stop()
        {
            if (!m_usingRAIN)
            {
                m_navmeshAgent.isStopped = true;
            }
        }

        public void MoveToPoint(Vector3 point, float speed)
        {
            m_navmeshAgent.speed = speed;
            m_navmeshAgent.SetDestination(point);
        }

        void GotoNextPoint()
        {
            // Returns if no points have been set up
            if (m_waypoints.Length == 0)
                return;

            // Set the agent to go to the currently selected destination.
            m_navmeshAgent.destination = m_waypoints[m_destPoint].position;

            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
            m_destPoint = (m_destPoint + 1) % m_waypoints.Length;
        }
        #endregion
    }

}