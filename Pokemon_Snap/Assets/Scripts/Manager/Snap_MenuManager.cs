﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Fungus;

namespace PokemonSnapUnity
{

    public class Snap_MenuManager : MonoBehaviour
    {
        private static Snap_MenuManager instance;
        public static Snap_MenuManager Instance { get { return instance; } }

        public enum MENU_STATES
        {
            PRESS_START,
            MAIN_MENU,
            OAK,
            STAGE,
            MENU_INGAME,
            OPTIONS,
            NAME_ENTRY,
            STAGE_SELECTION
        }

        [SerializeField] private MENU_STATES m_menuStates = MENU_STATES.PRESS_START;
        [SerializeField] private GameObject m_mainGroup;
        [SerializeField] private GameObject m_oakGroup;
        [SerializeField] private GameObject m_nameEntryGroup;
        [SerializeField] private GameObject m_levelSelectionGroup;
        [SerializeField] private GameObject m_mainPressStartGroup;
        [SerializeField] private GameObject m_mainMenuGroup;
        [SerializeField] private GameObject m_IngameMenuGroup;
        [SerializeField] private GameObject m_settingsGroup;
        [SerializeField] private GameObject m_loadingScreen;
        [SerializeField] private Image m_stagePreviewImg;
        [SerializeField] private Sprite[] m_stagePreviewImgs;

        [SerializeField] private string m_mainMenuBGM;
        [SerializeField] private string m_oakBGM;
        [SerializeField] private string m_stageSelectBGM;

        [SerializeField] private AudioSource m_buttonSource;
        [SerializeField] private AudioClip m_buttonEnter;
        [SerializeField] private AudioClip m_buttonConfirm;


        [Header(" ~ Texts to Parse ~ ")]
        [SerializeField] private Text m_pressStartText;
        [SerializeField] private Text m_continueButtonText;
        [SerializeField] private Text m_newGameButtonText;
        [SerializeField] private Text m_optionsButtonText;
        [SerializeField] private Text m_exitButtonText;
        [SerializeField] private Text m_optionsTitleText;
        [SerializeField] private Text m_languageText;
        [SerializeField] private Text m_vibrationText;
        [SerializeField] private Text m_contXSensText;
        [SerializeField] private Text m_contYSensText;
        [SerializeField] private Text m_mouseYSensText;
        [SerializeField] private Text m_mouseXSensText;
        [SerializeField] private Text m_invertYAxisText;
        [SerializeField] private Text m_applySettingsText;
        [SerializeField] private Text m_restoreSettingsText;
        [SerializeField] private Text m_backSettingsText;
        [SerializeField] private Text m_generalButtonText;
        [SerializeField] private Text m_graphicsButtonText;
        [SerializeField] private Text m_audioButtonText;
        [SerializeField] private Text m_qualityText;
        [SerializeField] private Text m_fullScreenText;
        [SerializeField] private Text m_resolutionText;
        [SerializeField] private Text m_vSyncText;
        [SerializeField] private Text m_masterVolumeText;
        [SerializeField] private Text m_musicVolumeText;
        [SerializeField] private Text m_effectVolumeText;
        [SerializeField] private Text m_ambientVolumeText;
        [SerializeField] private Text m_voiceVolumeText;
        [SerializeField] private Text m_confirmSettingsText;
        [SerializeField] private Text m_yesConfirmText;
        [SerializeField] private Text m_noConfirmText;
        [SerializeField] private Text m_nameEntryTitleText;
        [SerializeField] private Text m_nameEntryDescriptionText;
        [SerializeField] private Text m_acceptNameEntryText;
        [SerializeField] private Text m_backNameEntryText;
        [SerializeField] private Text m_nameEntryHelpText;
        [SerializeField] private Text m_warningNameEntryText;
        [SerializeField] private Text m_goToCourseText;
        [SerializeField] private Text m_goToAlbumText;
        [SerializeField] private Text m_saveGameText;
        [SerializeField] private Text m_resumeIngameText;
        [SerializeField] private Text m_settingsIngameText;
        [SerializeField] private Text m_goToLabIngameText;
        [SerializeField] private Text m_exitIngameText;
        [SerializeField] private Text m_loadingText;
        [SerializeField] private Text m_courseDescriptionText;
        [SerializeField] private Text m_beachCourseText;
        [SerializeField] private Text m_backCourseText;

        [Header(" ~ Fungus ~ ")]
        [SerializeField] private Flowchart m_oakFlowChart;
        [SerializeField] private Localization m_localization;
        private bool m_endedTutorial = false;

        public static bool m_pauseGame = false;
        public static bool m_initialAnimation = false;
        public static bool m_endGame = false;

        private bool m_inGame = false;

        public bool InGame { get { return m_inGame; } set { m_inGame = value; } }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this.gameObject);
            }

        }

        // Use this for initialization
        void Start()
        {
            m_pauseGame = false;

            Snap_DialogManager.Instance.InitializeDialogManager();
            ParseText();

            ShowPressStart();
        }

        // Update is called once per frame
        void Update()
        {
            switch (m_menuStates)
            {
                case MENU_STATES.PRESS_START:
                    if (Snap_InputManager.Instance && Snap_InputManager.Instance.SnapInput())
                    {
                        ShowMainMenu();
                    }
                    break;
            }

        }

        /// <summary>
        /// Parse all menú texts
        /// </summary>
        public void ParseText()
        {
            // INITIAL
            m_pressStartText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("pressStart");

            // MAIN MENU
            m_newGameButtonText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("newGameButton");
            m_continueButtonText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("continueButton");
            m_optionsButtonText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("optionsButton");
            m_exitButtonText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("exitButton");

            // GENERAL SETTINGS
            m_languageText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("languageSettings");
            m_vibrationText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("vibrationSettings");
            m_contXSensText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("contXSettings");
            m_contYSensText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("contYSettings");
            m_mouseXSensText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("mouseXSettings");
            m_mouseYSensText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("mouseYSettings");
            m_invertYAxisText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("invertYSettings");

            // GENERAL BUTTONS IN SETTINGS
            m_optionsTitleText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("optionsTitle");
            m_applySettingsText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("applySettings");
            m_restoreSettingsText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("restoreSettings");
            m_backSettingsText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("backSettings");
            m_generalButtonText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("generalButton");
            m_graphicsButtonText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("graphicsButton");
            m_audioButtonText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("audioButton");

            // GRAPHICS SETTINGS
            m_qualityText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("qualitySettings");
            m_fullScreenText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("fullScreenSettings");
            m_resolutionText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("resolutionsSettings");
            m_vSyncText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("vsyncSettings");

            // AUDIO SETTINGS
            m_masterVolumeText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("masterVolume");
            m_musicVolumeText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("musicVolume");
            m_effectVolumeText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("effectVolume");
            m_ambientVolumeText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("ambientVolume");
            m_voiceVolumeText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("voiceVolume");

            // CONFIRM PANEL
            m_confirmSettingsText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("confirmTextSettings");
            m_yesConfirmText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("yes");
            m_noConfirmText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("no");

            // NAME ENTRY
            m_nameEntryTitleText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("nameEntryTitle");
            m_nameEntryDescriptionText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("nameEntryDescription");
            m_acceptNameEntryText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("acceptMayusc");
            m_backNameEntryText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("backMayusc");
            m_nameEntryHelpText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("nameEntryHelpText");
            m_warningNameEntryText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("warningNameEntryText");

            // OAK'S LAB
            m_goToCourseText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("goToCourseButton");
            m_goToAlbumText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("albumButton");
            m_saveGameText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("saveGameButton");

            // INGAME
            m_resumeIngameText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("resumeIngameButton");
            m_goToLabIngameText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("goToLabIngameButton");
            m_exitIngameText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("exitIngameButton");
            m_settingsIngameText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("settingsIngameButton");

            // LOADING SCREEN
            m_loadingText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("loading");

            // COURSE SELECTION
            m_courseDescriptionText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("BeachDescription");
            m_beachCourseText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("Beach");
            m_backCourseText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("backSettings");

            ParseOakLanguage();
        }

        public void OnButtonEnter()
        {
            m_buttonSource.PlayOneShot(m_buttonEnter);
        }

        public void OnButtonPress()
        {
            m_buttonSource.PlayOneShot(m_buttonConfirm);
        }

        public void ParseOakLanguage()
        {
            m_localization.SetActiveLanguage(Snap_DialogManager.Instance.ParseXML.GetLang(), true);
            //m_oakFlowChart.SetStringVariable("language", Snap_DialogManager.Instance.ParseXML.GetLang());
        }

        public void ShowStageDescription(string key)
        {
            m_courseDescriptionText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo(key);
        }

        public void ShowStagePreview(int id)
        {
            m_stagePreviewImg.sprite = m_stagePreviewImgs[id];
        }

        public void ShowStageSelection()
        {
            m_menuStates = MENU_STATES.STAGE_SELECTION;
            m_oakGroup.SetActive(false);
            m_levelSelectionGroup.SetActive(true);

            Snap_AudioManager.Instance.PlayAudioItem(m_stageSelectBGM, true);
        }

        public void ShowPressStart()
        {
            m_menuStates = MENU_STATES.PRESS_START;
            m_mainGroup.SetActive(true);
            m_levelSelectionGroup.SetActive(false);
            m_oakGroup.SetActive(false);
            m_nameEntryGroup.SetActive(false);
            m_mainPressStartGroup.SetActive(true);
            m_mainMenuGroup.SetActive(false);
            m_settingsGroup.SetActive(false);
            m_IngameMenuGroup.SetActive(false);

            Snap_AudioManager.Instance.PlayAudioItem(m_mainMenuBGM);
        }

        public void ShowMainMenu()
        {
            m_menuStates = MENU_STATES.MAIN_MENU;
            m_mainPressStartGroup.SetActive(false);
            m_mainMenuGroup.SetActive(true);
            m_nameEntryGroup.SetActive(false);
            m_settingsGroup.SetActive(false);
        }

        public void ShowEntryName()
        {
            m_menuStates = MENU_STATES.NAME_ENTRY;
            m_mainMenuGroup.SetActive(false);
            m_nameEntryGroup.SetActive(true);
        }

        public void ShowSettings()
        {
            m_menuStates = MENU_STATES.OPTIONS;
            m_mainMenuGroup.SetActive(false);
            m_settingsGroup.SetActive(true);
        }

        public void CloseSettings()
        {
            if (!Snap_SettingsManager.Instance.CheckAnything())
            {
                if (m_inGame)
                {
                    m_menuStates = MENU_STATES.MENU_INGAME;
                    m_IngameMenuGroup.SetActive(true);
                }
                else
                {
                    m_menuStates = MENU_STATES.MAIN_MENU;
                    m_mainMenuGroup.SetActive(true);
                }
                m_settingsGroup.SetActive(false);
            }
        }

        public void GoBackFromSettings()
        {
            CloseConfirm();
            Snap_SettingsManager.Instance.BackDefault();
            Snap_SettingsManager.Instance.SetChangesBack();
            CloseSettings();
        }

        public void CloseConfirm()
        {
            Snap_SettingsManager.Instance.CloseConfirm();
        }

        public void ShowOaksLab(bool fromCourse = false)
        {
            m_inGame = false;
            m_menuStates = MENU_STATES.OAK;
            m_levelSelectionGroup.SetActive(false);
            m_oakGroup.SetActive(true);
            m_nameEntryGroup.SetActive(false);
            Snap_AudioManager.Instance.PlayAudioItem(m_oakBGM, true);

            m_endGame = false;
            m_pauseGame = false;
            m_initialAnimation = false;

            if (fromCourse && !m_endedTutorial)
                m_oakFlowChart.SendFungusMessage("backToOak");
        }

        public void MenuIngame()
        {
            m_pauseGame = !m_pauseGame;
            m_IngameMenuGroup.SetActive(m_pauseGame);
        }

        public void UnPauseIngame()
        {
            m_pauseGame = false;
            m_IngameMenuGroup.SetActive(m_pauseGame);
            Snap_PhotoManager.Instance.ContinueGame();
        }

        public void NewLanguageData()
        {

        }
        public void BackFromCourseLevel()
        {
            ShowOaksLab(true);
        }

        public void GoBackToOakScene(string level_name = "Menu")
        { 
            if (Snap_PhotoManager.Instance)
                Snap_PhotoManager.Instance.DeletePics();

            StartCoroutine(LoadSceneOak(level_name));
        }

        IEnumerator LoadSceneOak(string scene = "Menu", float delay = 0f)
        {
            Snap_AudioManager.Instance.ClearForChangingScene();
            m_loadingScreen.SetActive(true);
            m_IngameMenuGroup.SetActive(false);
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            yield return new WaitForSeconds(delay);
            m_loadingScreen.SetActive(false);
            m_mainGroup.SetActive(true);

            ShowOaksLab(true);
        }

        public void GoToGame(string level_name = "Beach")
        {
            m_menuStates = MENU_STATES.STAGE;
            m_inGame = true;
            Snap_AudioManager.Instance.StopAudioItem(m_oakBGM);
            m_mainGroup.SetActive(false);
            m_levelSelectionGroup.SetActive(false);
            StartCoroutine(LoadScene(level_name));
        }

        IEnumerator LoadScene(string scene = "Beach", float delay = 0f)
        {
            Snap_AudioManager.Instance.ClearForChangingScene();
            m_loadingScreen.SetActive(true);
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            yield return new WaitForSeconds(delay);
            m_loadingScreen.SetActive(false);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }

}