﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PokemonSnapUnity
{

    /// <summary>
    /// All the GameSettings
    /// </summary>
    public class Snap_SettingsManager : MonoBehaviour
    {
        /// <summary>
        /// Singleton
        /// </summary>
        private static Snap_SettingsManager instance;
        public static Snap_SettingsManager Instance { get { return instance; } }

        [Header("General Settings")]
        /// <summary>
        /// Game Languages array
        /// </summary>
        [SerializeField]
        private SystemLanguage[] m_gameLanguages;
        /// <summary>
        /// Current GameLanguage in Unity Enumeration
        /// </summary>
        [SerializeField] private SystemLanguage m_gameLanguage;
        /// <summary>
        /// Index from the dropdown of languages
        /// </summary>
        public int m_LanguageIndex = 0;
        /// <summary>
        /// Default language (0 = English)
        /// </summary>
        public int m_defaultLanguage = 0;
        /// <summary>
        /// Invert Y Axis boolean
        /// </summary>
        [SerializeField] private bool m_invertYAxis = false;
        /// <summary>
        /// Default sensibility
        /// </summary>
        [SerializeField] [Range(0, 5)] private float m_defaultSensibility = 2f;
        /// <summary>
        /// Sensibility for X Axis of the mouse
        /// </summary>
        [SerializeField] private float m_xSensibilityMouse = 2f;
        /// <summary>
        /// Sensibility for Y Axis of the mouse
        /// </summary>
        [SerializeField] private float m_ySensibilityMouse = 2f;
        /// <summary>
        /// Controller vibration activated or not
        /// </summary>
        [SerializeField] private bool m_controllerVibration = false;
        /// <summary>
        /// Sensibility for X Axis of the Controller
        /// </summary>
        [SerializeField] private float m_xSensibilityController = 2f;
        /// <summary>
        /// Sensibility for Y Axis of the Controller
        /// </summary>
        [SerializeField] private float m_ySensibilityController = 2f;
        [SerializeField] private float m_maxSensibility = 5f;
        [SerializeField] private int m_settingPanelShowing = 1;

        [Header("Graphics Settings")]
        [SerializeField]
        private bool m_fullScreen = true;
        [SerializeField] private bool m_vSync = true;
        [SerializeField] private int m_fullScreenIndex = 0;
        [SerializeField] private int m_vSyncIndex = 0;
        [SerializeField] private int m_defaultQualityIndex = -1;
        [SerializeField] private int m_qualityIndex = 0;
        [SerializeField] private int m_defaultResolutionIndex = -1;
        [SerializeField] private int m_resolutionIndex = 0;
        [SerializeField] private string m_resolutionName;
        [SerializeField] private string m_qualityName;

        [Header("Audio Settings")]
        [SerializeField]
        private float m_masterVolume = 1f;
        [SerializeField] private float m_sfxVolume = 1f;
        [SerializeField] private float m_musicVolume = 1f;
        [SerializeField] private float m_ambientVolume = 1f;
        [SerializeField] private float m_voicesVolume = 1f;
        [SerializeField] private bool m_audioManagerType = true;

        [Header("References")]
        [SerializeField]
        private GameObject m_confirmOptions;
        [SerializeField] private GameObject m_InputManagerPrefab;

        [Header("General References")]
        [SerializeField]
        private GameObject m_generalPanel;
        [SerializeField] private Dropdown m_languageDropdown;
        [SerializeField] private Slider m_mouseXSensSlider;
        [SerializeField] private Slider m_mouseYSensSlider;
        [SerializeField] private Slider m_controllerXSensSlider;
        [SerializeField] private Slider m_controllerYSensSlider;
        [SerializeField] private Toggle m_invertYAxisToggle;
        [SerializeField] private Toggle m_vibrationToggle;

        [Header("Graphics References")]
        [SerializeField]
        private GameObject m_graphicsPanel;
        [SerializeField] private Text m_GPUNames;
        [SerializeField] private Toggle m_fullScreenToggle;
        [SerializeField] private Toggle m_vSyncToggle;
        [SerializeField] private Dropdown m_qualityDropdown;
        [SerializeField] private Dropdown m_resolutionDropdown;
        [SerializeField] private List<string> resolutionsString;
        private Resolution[] resolutions;

        [Header("Audio References")]
        [SerializeField]
        private GameObject m_audioPanel;
        [SerializeField] private Slider m_masterVolumeSlider;
        [SerializeField] private Slider m_sfxVolumeSlider;
        [SerializeField] private Slider m_ambientVolumeSlider;
        [SerializeField] private Slider m_musicVolumeSlider;
        [SerializeField] private Slider m_voiceVolumeSlider;

        [Header("Ingame References")]
        [SerializeField]
        private GameObject m_inGameGroup;
        [SerializeField] private GameObject m_inGamePanelMain;


        [Header("General Changed Settings")]
        [SerializeField]
        private bool m_changedAnything = false;
        [SerializeField] private bool m_changedLanguage = false;
        [SerializeField] private bool m_changedYAxis = false;
        [SerializeField] private bool m_changedControllerVibration = false;
        [SerializeField] private bool m_changedMouseSensitivity = false;
        [SerializeField] private bool m_changedControllerSensitivity = false;

        [Header("Graphics Changed Settings")]
        [SerializeField]
        private bool m_changedFullScreen = false;
        [SerializeField] private bool m_changedVSync = false;
        [SerializeField] private bool m_changedQuality = false;
        [SerializeField] private bool m_changedResolution = false;

        [Header("Audio Changed Settings")]
        [SerializeField]
        private bool m_changedMasterVolume = false;
        [SerializeField] private bool m_changedAmbientVolume = false;
        [SerializeField] private bool m_changedMusicVolume = false;
        [SerializeField] private bool m_changedVoicesVolume = false;
        [SerializeField] private bool m_changedSfxVolume = false;

        [Header("Other Settings")]
        [SerializeField]
        private bool m_destroyPlayerPrefs = false;

        /// <summary>
        /// Properties
        /// </summary>
        public int GameLanguageIndex { set { m_LanguageIndex = value; } get { return m_LanguageIndex; } }
        public SystemLanguage GameLanguage { set { m_gameLanguage = value; } get { return m_gameLanguage; } }

        public bool ChangedMasterVolume { set { m_changedMasterVolume = value; } get { return m_changedMasterVolume; } }
        public bool ChangedAmbientVolume { set { m_changedAmbientVolume = value; } get { return m_changedAmbientVolume; } }
        public bool ChangedMusicVolume { set { m_changedMusicVolume = value; } get { return m_changedMusicVolume; } }
        public bool ChangedVoicesVolume { set { m_changedVoicesVolume = value; } get { return m_changedVoicesVolume; } }
        public bool ChangedSfxVolume { set { m_changedSfxVolume = value; } get { return m_changedSfxVolume; } }

        public bool InvertYAxis { set { m_invertYAxis = value; } get { return m_invertYAxis; } }
        public bool ControllerVibration { set { m_controllerVibration = value; } get { return m_controllerVibration; } }

        public bool ChangedAnything { set { m_changedAnything = value; } get { return m_changedAnything; } }

        public bool ChangedVibration { set { m_changedControllerVibration = value; } get { return m_changedControllerVibration; } }
        public bool ChangedLanguage { set { m_changedLanguage = value; } get { return m_changedLanguage; } }
        public bool ChangedYAxis { set { m_changedYAxis = value; } get { return m_changedYAxis; } }
        public bool ChangedMouseSensitivity { set { m_changedMouseSensitivity = value; } get { return m_changedMouseSensitivity; } }
        public bool ChangedControllerSensitivity { set { m_changedControllerSensitivity = value; } get { return m_changedControllerSensitivity; } }

        public bool ChangedFullScreen { set { m_changedFullScreen = value; } get { return m_changedFullScreen; } }
        public bool ChangedVSync { set { m_changedVSync = value; } get { return m_changedVSync; } }
        public bool ChangedQuality { set { m_changedQuality = value; } get { return m_changedQuality; } }
        public bool ChangedResolution { set { m_changedResolution = value; } get { return m_changedResolution; } }

        public float MouseXSensibility { set { m_xSensibilityMouse = value; } get { return m_xSensibilityMouse; } }
        public float MouseYSensibility { set { m_ySensibilityMouse = value; } get { return m_ySensibilityMouse; } }
        public float ControllerXSensibility { set { m_xSensibilityController = value; } get { return m_xSensibilityController; } }
        public float ControllerYSensibility { set { m_ySensibilityController = value; } get { return m_ySensibilityController; } }

        public float MasterVolume { set { m_masterVolume = value; } get { return m_masterVolume; } }
        public float SfxVolume { set { m_sfxVolume = value; } get { return m_sfxVolume; } }
        public float AmbientVolume { set { m_ambientVolume = value; } get { return m_ambientVolume; } }
        public float MusicVolume { set { m_musicVolume = value; } get { return m_musicVolume; } }
        public float VoiceVolume { set { m_voicesVolume = value; } get { return m_voicesVolume; } }

        public bool AudioType { set { m_audioManagerType = value; } get { return m_audioManagerType; } }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this);

                SetDefaultValues();
            }
            else
            {
                Debug.LogError("[Settings Manager] Error! Se está creando una segunda instancia de Settings Manager");
                Destroy(this.gameObject);
            }
        }

        public void BackDefault()
        {
            switch (m_settingPanelShowing)
            {
                case 0:
                case 1:
                    SetDefaultGeneralSettings();
                    break;
                case 2:
                    SetDefaultGraphicsSettings();
                    break;
                case 3:
                    SetDefaultAudioSettings();
                    break;
                default:
                    SetDefaultGeneralSettings();
                    break;
            }
        }

        public void SetDefaultGeneralSettings()
        {
            /// GENERAL
            if (m_changedLanguage)
            {
                m_LanguageIndex = PlayerPrefs.GetInt("languageIndex", m_defaultLanguage);
                m_gameLanguage = m_gameLanguages[m_LanguageIndex];
                m_languageDropdown.value = m_LanguageIndex;
            }

            if (m_changedYAxis)
            {
                m_invertYAxis = (PlayerPrefs.GetInt("InvertYAxis", 0) == 0) ? false : true;
                m_invertYAxisToggle.isOn = m_invertYAxis;
            }

            if (m_changedControllerVibration)
            {
                m_controllerVibration = (PlayerPrefs.GetInt("ControllerVibration", 1) == 0) ? false : true;
                m_vibrationToggle.isOn = m_controllerVibration;
            }

            if (m_changedControllerSensitivity)
            {
                m_xSensibilityController = PlayerPrefs.GetFloat("xSensibilityController", m_defaultSensibility);
                m_ySensibilityController = PlayerPrefs.GetFloat("ySensibilityController", m_defaultSensibility);

                m_controllerXSensSlider.value = m_xSensibilityController;
                m_controllerYSensSlider.value = m_ySensibilityController;
            }

            if (m_changedMouseSensitivity)
            {
                m_xSensibilityMouse = PlayerPrefs.GetFloat("xSensibilityMouse", m_defaultSensibility);
                m_ySensibilityMouse = PlayerPrefs.GetFloat("ySensibilityMouse", m_defaultSensibility);

                m_mouseXSensSlider.value = m_xSensibilityMouse;
                m_mouseYSensSlider.value = m_ySensibilityMouse;
            }
        }

        public void SetDefaultGraphicsSettings()
        {
            if (m_changedQuality)
            {
                m_qualityIndex = PlayerPrefs.GetInt("qualityLevel", QualitySettings.GetQualityLevel());
                m_qualityDropdown.value = m_qualityIndex;
            }

            if (m_changedVSync)
            {
                //Vsync option
                m_vSyncIndex = PlayerPrefs.GetInt("vsync", QualitySettings.vSyncCount);
                if (m_vSyncIndex == 0)
                {
                    m_vSync = false;
                    m_vSyncToggle.isOn = false;
                }
                else
                {
                    m_vSync = true;
                    m_vSyncToggle.isOn = true;
                }
            }

            if (m_changedFullScreen)
            {
                // FullScreen
                m_fullScreenIndex = PlayerPrefs.GetInt("fullScreen", 0);
                m_fullScreen = (m_fullScreenIndex == 0) ? true : false;
                m_fullScreenToggle.isOn = m_fullScreen;
            }

            if (m_changedResolution)
            {
                m_resolutionIndex = PlayerPrefs.GetInt("resolution", resolutions.Length - 1);
                m_resolutionDropdown.value = m_resolutionIndex;
            }
        }

        public void SetDefaultAudioSettings()
        {
            if (m_changedMasterVolume)
            {
                m_masterVolume = PlayerPrefs.GetFloat("masterVolume", 1f);
                m_masterVolumeSlider.value = m_masterVolume;
            }

            if (m_changedAmbientVolume)
            {
                m_ambientVolume = PlayerPrefs.GetFloat("ambientVolume", 1f);
                m_ambientVolumeSlider.value = m_ambientVolume;
            }

            if (m_changedSfxVolume)
            {
                m_sfxVolume = PlayerPrefs.GetFloat("sfxVolume", 1f);
                m_sfxVolumeSlider.value = m_sfxVolume;
            }

            if (m_changedVoicesVolume)
            {
                m_voicesVolume = PlayerPrefs.GetFloat("voiceVolume", 1f);
                m_voiceVolumeSlider.value = m_voicesVolume;
            }

            if (m_changedMusicVolume)
            {
                m_musicVolume = PlayerPrefs.GetFloat("musicVolume", 1f);
                m_musicVolumeSlider.value = m_musicVolume;
            }
        }

        void SetDefaultValues()
        {
            if (m_destroyPlayerPrefs)
                PlayerPrefs.DeleteAll();

            /// GENERAL
            m_defaultLanguage = (Application.systemLanguage == SystemLanguage.English) ? 0 : 1;

            m_LanguageIndex = PlayerPrefs.GetInt("languageIndex", m_defaultLanguage);
            m_gameLanguage = m_gameLanguages[m_LanguageIndex];

            m_languageDropdown.value = m_LanguageIndex;

            m_invertYAxis = (PlayerPrefs.GetInt("InvertYAxis", 0) == 0) ? false : true;
            m_controllerVibration = (PlayerPrefs.GetInt("ControllerVibration", 1) == 0) ? false : true;

            m_xSensibilityMouse = PlayerPrefs.GetFloat("xSensibilityMouse", m_defaultSensibility);
            m_ySensibilityMouse = PlayerPrefs.GetFloat("ySensibilityMouse", m_defaultSensibility);

            m_mouseXSensSlider.maxValue = m_maxSensibility;
            m_mouseXSensSlider.minValue = 0f;
            m_mouseYSensSlider.maxValue = m_maxSensibility;
            m_mouseYSensSlider.minValue = 0f;

            m_mouseXSensSlider.value = m_xSensibilityMouse;
            m_mouseYSensSlider.value = m_ySensibilityMouse;

            m_xSensibilityController = PlayerPrefs.GetFloat("xSensibilityController", m_defaultSensibility);
            m_ySensibilityController = PlayerPrefs.GetFloat("ySensibilityController", m_defaultSensibility);

            m_controllerXSensSlider.maxValue = m_maxSensibility;
            m_controllerXSensSlider.minValue = 0f;
            m_controllerYSensSlider.maxValue = m_maxSensibility;
            m_controllerYSensSlider.minValue = 0f;

            m_controllerXSensSlider.value = m_xSensibilityController;
            m_controllerYSensSlider.value = m_ySensibilityController;

            m_vibrationToggle.isOn = m_controllerVibration;
            m_invertYAxisToggle.isOn = m_invertYAxis;

            m_settingPanelShowing = 1;

            /// GRAPHICS
            m_GPUNames.text = SystemInfo.graphicsDeviceName;

            m_qualityIndex = PlayerPrefs.GetInt("qualityLevel", QualitySettings.GetQualityLevel());
            m_defaultQualityIndex = PlayerPrefs.GetInt("defaultQualityLevel", -1);

            // first time
            if (m_defaultQualityIndex == -1)
            {
                PlayerPrefs.SetInt("defaultQualityLevel", QualitySettings.GetQualityLevel());
                m_defaultQualityIndex = QualitySettings.GetQualityLevel();
            }

            m_qualityDropdown.value = m_qualityIndex;
            QualitySettings.SetQualityLevel(m_qualityIndex, true);

            m_vSyncIndex = PlayerPrefs.GetInt("vsync", QualitySettings.vSyncCount);

            QualitySettings.vSyncCount = m_vSyncIndex;

            if (m_vSyncIndex == 0)
            {
                m_vSync = false;
                m_vSyncToggle.isOn = false;
            }
            else //if (QualitySettings.vSyncCount == 1)
            {
                m_vSync = true;
                m_vSyncToggle.isOn = true;
            }

            // FullScreen
            m_fullScreenIndex = PlayerPrefs.GetInt("fullScreen", 0);
            m_fullScreen = (m_fullScreenIndex == 0) ? true : false;
            Screen.fullScreen = m_fullScreen;
            m_fullScreenToggle.isOn = m_fullScreen;

            // Resolutions
            resolutions = Screen.resolutions;
            resolutionsString = new List<string>();
            m_resolutionIndex = PlayerPrefs.GetInt("resolution", resolutions.Length - 1);

            foreach (Resolution res in resolutions)
            {
                resolutionsString.Add(res.ToString());
            }

            m_resolutionDropdown.ClearOptions();
            m_resolutionDropdown.AddOptions(resolutionsString);

            m_resolutionDropdown.value = m_resolutionIndex;
            m_defaultResolutionIndex = m_resolutionIndex;
            Screen.SetResolution(resolutions[m_resolutionIndex].width, resolutions[m_resolutionIndex].height, m_fullScreen);

            /// AUDIO
            m_masterVolume = PlayerPrefs.GetFloat("masterVolume", 1f);
            m_sfxVolume = PlayerPrefs.GetFloat("sfxVolume", 1f);
            m_ambientVolume = PlayerPrefs.GetFloat("ambientVolume", 1f);
            m_musicVolume = PlayerPrefs.GetFloat("musicVolume", 1f);
            m_voicesVolume = PlayerPrefs.GetFloat("voiceVolume", 1f);

            m_masterVolumeSlider.value = m_masterVolume;
            m_sfxVolumeSlider.value = m_sfxVolume;
            m_ambientVolumeSlider.value = m_ambientVolume;
            m_musicVolumeSlider.value = m_musicVolume;
            m_voiceVolumeSlider.value = m_voicesVolume;

            if (Snap_AudioManager.Instance)
                Snap_AudioManager.Instance.SetVolumes(m_masterVolume, m_sfxVolume, m_ambientVolume, m_musicVolume, m_voicesVolume);
        }


        public void ParsePauseMenu()
        {

        }

        public void PauseIngame()
        {
            m_inGameGroup.SetActive(true);
            m_inGamePanelMain.SetActive(true);
        }

        public void UnPauseIngame()
        {
            m_inGameGroup.SetActive(false);
            m_inGamePanelMain.SetActive(false);
        }


        // Use this for initialization
        void Start()
        {
            SetChangesBack();
        }



        // Update is called once per frame
        void Update()
        {

        }

        public void OpenEntryName()
        {

        }

        public void ActivateSimpleMenu()
        {
            Snap_MenuManager.Instance.ShowMainMenu();
        }

        public void ActivateSimpleMenuIngame()
        {
            Snap_MenuManager.Instance.MenuIngame();
        }

        public void SetPanelSettings(int value)
        {
            m_settingPanelShowing = value;

            switch (value)
            {
                case 0:
                case 1:
                    m_generalPanel.SetActive(true);
                    m_graphicsPanel.SetActive(false);
                    m_audioPanel.SetActive(false);
                    break;
                case 2:
                    m_generalPanel.SetActive(false);
                    m_graphicsPanel.SetActive(true);
                    m_audioPanel.SetActive(false);
                    break;
                case 3:
                    m_generalPanel.SetActive(false);
                    m_graphicsPanel.SetActive(false);
                    m_audioPanel.SetActive(true);
                    break;
                default:
                    m_generalPanel.SetActive(true);
                    m_graphicsPanel.SetActive(false);
                    m_audioPanel.SetActive(false);
                    break;
            }
        }

        public void SetLanguage()
        {
            int value = m_languageDropdown.value;
            PlayerPrefs.SetInt("languageIndex", value);
            m_LanguageIndex = value;
            m_gameLanguage = m_gameLanguages[m_LanguageIndex];
            Snap_DialogManager.Instance.ParseXML.SetLang(m_gameLanguage);
            Debug.Log(m_gameLanguage);
            Snap_MenuManager.Instance.ParseText();
            m_changedLanguage = false;
        }

        public void SetMouseSensitivity()
        {
            m_changedMouseSensitivity = false;

            m_xSensibilityMouse = m_mouseXSensSlider.value;
            PlayerPrefs.SetFloat("xSensibilityMouse", m_xSensibilityMouse);

            m_ySensibilityMouse = m_mouseYSensSlider.value;
            PlayerPrefs.SetFloat("ySensibilityMouse", m_ySensibilityMouse);

            if (Snap_PhotoManager.Instance)
            {
                Snap_PhotoManager.Instance.PlayerScript.MouseLook.SetMouseSensibility(m_xSensibilityMouse, m_ySensibilityMouse);
            }
        }

        public void SetControllerSensitivity()
        {
            m_changedControllerSensitivity = false;

            m_xSensibilityController = m_controllerXSensSlider.value;
            PlayerPrefs.SetFloat("xSensibilityController", m_xSensibilityController);

            m_ySensibilityController = m_controllerYSensSlider.value;
            PlayerPrefs.SetFloat("ySensibilityController", m_ySensibilityController);

            if (Snap_PhotoManager.Instance)
            {
                Snap_PhotoManager.Instance.PlayerScript.MouseLook.SetControllerSensibility(m_xSensibilityController, m_ySensibilityController);
            }
        }

        public void SetVibration()
        {
            m_controllerVibration = m_vibrationToggle.isOn;
            m_changedControllerVibration = false;

            int value = (m_controllerVibration) ? 1 : 0;
            PlayerPrefs.SetInt("ControllerVibration", value);

            if (Snap_InputManager.Instance)
            {
                Debug.Log(m_controllerVibration);
                Snap_InputManager.Instance.CanVibrate = m_controllerVibration;
            }
        }

        public void SetVSync()
        {
            m_vSync = m_vSyncToggle.isOn;
            m_changedVSync = false;

            int value = (m_vSync) ? 1 : 0;
            PlayerPrefs.SetInt("vsync", value);

            m_vSyncIndex = value;

            QualitySettings.vSyncCount = m_vSyncIndex;
        }

        public void SetFullScreen()
        {
            m_fullScreen = m_fullScreenToggle.isOn;
            m_changedFullScreen = false;

            int value = (m_fullScreen) ? 0 : 1;
            PlayerPrefs.SetInt("fullScreen", value);

            m_fullScreenIndex = value;

            Screen.fullScreen = m_fullScreen;
        }

        public void SetResolution()
        {
            m_changedResolution = false;
            m_resolutionIndex = m_resolutionDropdown.value;
            m_resolutionName = resolutions[m_resolutionIndex].ToString();
            PlayerPrefs.SetInt("resolution", m_resolutionIndex);
            Screen.SetResolution(resolutions[m_resolutionIndex].width, resolutions[m_resolutionIndex].height, m_fullScreen);
        }

        public void SetYAxisInvert()
        {
            m_invertYAxis = m_invertYAxisToggle.isOn;
            m_changedYAxis = false;

            int value = (m_invertYAxis) ? 1 : 0;
            PlayerPrefs.SetInt("InvertYAxis", value);

            if (Snap_PhotoManager.Instance)
            {
                Snap_PhotoManager.Instance.PlayerScript.MouseLook.SetInvertYAxis(m_invertYAxis);
            }
        }

        public void QualityPreview()
        {
            int preview = m_qualityIndex;
            m_qualityIndex = m_qualityDropdown.value;

            QualitySettings.SetQualityLevel(m_qualityIndex);

            m_qualityIndex = preview;
            m_changedQuality = true;
        }

        public void SetQuality()
        {
            m_changedQuality = false;
            m_qualityIndex = m_qualityDropdown.value;

            PlayerPrefs.SetInt("qualityLevel", m_qualityIndex);
            QualitySettings.SetQualityLevel(m_qualityIndex);
        }

        public void changedSomething(bool value)
        {
            m_changedAnything = value;
        }

        public bool CheckAnything()
        {
            if (m_changedAnything)
            {
                m_confirmOptions.SetActive(true);
                return true;
            }
            return false;
        }

        public void CloseConfirm()
        {
            m_confirmOptions.SetActive(false);
        }

        public void SetMasterVolume()
        {
            m_changedMasterVolume = false;
            m_masterVolume = m_masterVolumeSlider.value;
            if (m_audioManagerType)
            {
                if (Snap_AudioManager.Instance)
                {
                    Snap_AudioManager.Instance.MasterVolume = m_masterVolume;
                    Snap_AudioManager.Instance.UpdateAudioVolume();
                }
            }

            PlayerPrefs.SetFloat("masterVolume", m_masterVolume);
        }

        public void SetMusicVolume()
        {
            m_changedMusicVolume = false;
            m_musicVolume = m_musicVolumeSlider.value;
            if (m_audioManagerType)
            {
                if (Snap_AudioManager.Instance)
                {
                    Snap_AudioManager.Instance.MusicVolume = m_musicVolume;
                    Snap_AudioManager.Instance.UpdateAudioVolume();
                }

            }
            PlayerPrefs.SetFloat("musicVolume", m_musicVolume);
        }

        public void SetAmbientVolume()
        {
            m_changedAmbientVolume = false;
            m_ambientVolume = m_ambientVolumeSlider.value;
            if (m_audioManagerType)
            {
                if (Snap_AudioManager.Instance)
                {
                    Snap_AudioManager.Instance.AmbientVolume = m_ambientVolume;
                    Snap_AudioManager.Instance.UpdateAudioVolume();
                }

            }
            PlayerPrefs.SetFloat("ambientVolume", m_ambientVolume);
        }

        public void SetEffectVolume()
        {
            m_changedSfxVolume = false;
            m_sfxVolume = m_sfxVolumeSlider.value;
            if (m_audioManagerType)
            {
                if (Snap_AudioManager.Instance)
                {
                    Snap_AudioManager.Instance.EffectsVolume = m_sfxVolume;
                    Snap_AudioManager.Instance.UpdateAudioVolume();
                }

            }
            PlayerPrefs.SetFloat("sfxVolume", m_sfxVolume);
        }

        public void SetVoiceVolume()
        {
            m_changedVoicesVolume = false;
            m_voicesVolume = m_voiceVolumeSlider.value;
            if (m_audioManagerType)
            {
                if (Snap_AudioManager.Instance)
                {
                    Snap_AudioManager.Instance.VoiceVolume = m_voicesVolume;
                    Snap_AudioManager.Instance.UpdateAudioVolume();
                }

            }
            PlayerPrefs.SetFloat("voiceVolume", m_voicesVolume);
        }

        /// <summary>
        /// Apply changed data
        /// </summary>
        public void ApplyData()
        {
            m_changedAnything = false;

            if (m_changedLanguage)
                SetLanguage();

            if (m_changedControllerVibration)
                SetVibration();

            if (m_changedYAxis)
                SetYAxisInvert();

            if (m_changedControllerSensitivity)
                SetControllerSensitivity();

            if (m_changedMouseSensitivity)
                SetMouseSensitivity();

            if (m_changedResolution)
                SetResolution();

            if (m_changedVSync)
                SetVSync();

            if (m_changedFullScreen)
                SetFullScreen();

            if (m_changedQuality)
                SetQuality();

            if (m_changedVoicesVolume)
                SetVoiceVolume();

            if (m_changedSfxVolume)
                SetEffectVolume();

            if (m_changedMusicVolume)
                SetMusicVolume();

            if (m_changedMasterVolume)
                SetMasterVolume();

            if (m_changedAmbientVolume)
                SetAmbientVolume();
        }
        /// <summary>
        /// Restore Default Data (Not applied 'til clicking Apply)
        /// </summary>
        public void RestoreDefaultData()
        {
            switch (m_settingPanelShowing)
            {
                case 1:
                    m_LanguageIndex = m_defaultLanguage;
                    m_gameLanguage = SystemLanguage.Spanish;
                    m_languageDropdown.value = m_LanguageIndex;
                    m_invertYAxis = false;
                    m_controllerVibration = true;
                    m_xSensibilityMouse = m_defaultSensibility;
                    m_ySensibilityMouse = m_defaultSensibility;
                    m_xSensibilityController = m_defaultSensibility;
                    m_ySensibilityController = m_defaultSensibility;
                    m_mouseXSensSlider.value = m_xSensibilityMouse;
                    m_mouseYSensSlider.value = m_ySensibilityMouse;
                    m_controllerXSensSlider.value = m_xSensibilityController;
                    m_controllerYSensSlider.value = m_ySensibilityController;

                    m_invertYAxisToggle.isOn = false;
                    m_vibrationToggle.isOn = true;

                    m_changedLanguage = true;
                    m_changedYAxis = true;
                    m_changedControllerVibration = true;
                    m_changedMouseSensitivity = true;
                    m_changedControllerSensitivity = true;

                    break;
                case 2:
                    m_changedQuality = true;
                    m_changedVSync = true;
                    m_changedFullScreen = true;
                    m_changedResolution = true;

                    m_fullScreenToggle.isOn = true;
                    m_vSyncToggle.isOn = true;

                    m_qualityIndex = m_defaultResolutionIndex;
                    m_qualityDropdown.value = m_defaultQualityIndex;

                    m_resolutionDropdown.value = m_defaultResolutionIndex;
                    break;
                case 3:
                    m_changedAmbientVolume = true;
                    m_changedMasterVolume = true;
                    m_changedSfxVolume = true;
                    m_changedVoicesVolume = true;
                    m_changedMusicVolume = true;

                    m_masterVolume = 1f;
                    m_masterVolumeSlider.value = m_masterVolume;

                    m_sfxVolume = 1f;
                    m_sfxVolumeSlider.value = m_sfxVolume;

                    m_ambientVolume = 1f;
                    m_ambientVolumeSlider.value = m_ambientVolume;

                    m_musicVolume = 1f;
                    m_musicVolumeSlider.value = m_musicVolume;

                    m_voicesVolume = 1f;
                    m_voiceVolumeSlider.value = m_voicesVolume;
                    break;
            }
        }

        public void SetChangesBack()
        {
            m_settingPanelShowing = 1;
            m_changedLanguage = false;
            m_changedYAxis = false;
            m_changedControllerVibration = false;
            m_changedMouseSensitivity = false;
            m_changedControllerSensitivity = false;
            m_changedAnything = false;

            m_changedFullScreen = false;
            m_changedQuality = false;
            m_changedResolution = false;
            m_changedVSync = false;

            m_changedAmbientVolume = false;
            m_changedMasterVolume = false;
            m_changedSfxVolume = false;
            m_changedVoicesVolume = false;
            m_changedMusicVolume = false;
        }

    }

}