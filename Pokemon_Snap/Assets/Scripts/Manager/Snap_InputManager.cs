﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure; // Required in C#
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

namespace PokemonSnapUnity
{

    public class Snap_InputManager : MonoBehaviour
    {
        private static Snap_InputManager instance;
        public static Snap_InputManager Instance { get { return instance; } }

        [Header("Controller Settings")]
        /// <summary>
        /// If the manager is destroyed or not when loading a new scene
        /// </summary>
        [SerializeField]
        private bool m_destroyOnLoad = false;
        /// <summary>
        /// Name of the current connected controller
        /// </summary>
        [SerializeField] private string m_ControllerName = "";
        /// <summary>
        /// Connected controller checker
        /// </summary>
        [SerializeField] private bool m_bIsControllerConnected = false;
        /// <summary>
        /// Names for controllers. Just configured two of them
        /// </summary>
        [SerializeField] private string[] m_controllerNames = { "Wireless Controller", "Controller (XBOX 360 For Windows)" };   // 0 - PS4, 1 - XBOX 360
                                                                                                                                /// <summary>
                                                                                                                                /// Checker for knowing if the player is using a controller
                                                                                                                                /// </summary>
        [SerializeField] private bool m_usingController = false;
        /// <summary>
        /// Checker for knowing if the controller is vibrating
        /// </summary>
        [SerializeField] private bool m_vibrating = false;
        /// <summary>
        /// If the controller can vibrate or not
        /// </summary>
        [SerializeField] private bool m_canVibrate = false;
        /// <summary>
        /// Xbox 360 left controller
        /// </summary>
        [SerializeField] private float m_leftMotorVibration = .1f;
        /// <summary>
        /// Xbox 360 right controller
        /// </summary>
        [SerializeField] private float m_rightMotorVibration = .1f;
        /// <summary>
        /// Time of vibration
        /// </summary>
        [SerializeField] private float m_vibrationTime = 0.0f;
        /// <summary>
        /// GamePad State
        /// </summary>
        GamePadState state;
        GamePadState prevState;
        PlayerIndex playerIndex;

        public bool IsUsingController { get { return m_usingController; } }
        public bool IsControllerConnected { get { return m_bIsControllerConnected; } }
        public bool CanVibrate { set { m_canVibrate = value; } get { return m_canVibrate; } }
        public bool Vibrating { set { m_vibrating = value; } }
        public string ControllerName { get { return m_ControllerName; } }
        public float LeftMotorVibration { set { m_leftMotorVibration = value; } }
        public float RightMotorVibration { set { m_rightMotorVibration = value; } }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;

                if (!m_destroyOnLoad)
                    DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        void Start()
        {
            if (Input.GetJoystickNames().Length > 0)
                m_bIsControllerConnected = true;
            else
                m_bIsControllerConnected = false;

            m_usingController = m_bIsControllerConnected;
            playerIndex = PlayerIndex.One;

            m_canVibrate = (PlayerPrefs.GetInt("ControllerVibration", 1) == 0) ? false : true;
        }

        private void Update()
        {
            prevState = state;
            state = GamePad.GetState(playerIndex);

            m_bIsControllerConnected = state.IsConnected;

            if (isControllerInput())
            {
                m_usingController = true;
            }
            else if (isMouseKeyboard())
            {
                m_usingController = false;
            }

            if (m_vibrating)
            {
                if (m_vibrationTime > 0.0)
                {
                    m_vibrationTime -= Time.deltaTime;
                }
                else
                {
                    StopVibration();
                }
            }
        }

        private void FixedUpdate()
        {
            if (m_bIsControllerConnected && m_vibrating)
                GamePad.SetVibration(0, m_leftMotorVibration, m_rightMotorVibration);
        }

        /// <summary>
        /// Check if keyboard buttons are pressed
        /// </summary>
        /// <returns></returns>
        private bool isMouseKeyboard()
        {
            // mouse movement
            if (Input.GetAxis("Mouse X") != 0.0f ||
                Input.GetAxis("Mouse Y") != 0.0f ||
                Input.GetAxis("Mouse ScrollWheel") != 0.0f)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Check if controller buttons are pressed
        /// </summary>
        /// <returns></returns>
        private bool isControllerInput()
        {
            if (!m_bIsControllerConnected)
                return false;

            // joystick axis
            if (Input.GetAxis("Horizontal") != 0.0f ||
               Input.GetAxis("Vertical") != 0.0f)
            {
                return true;
            }

            return false;
        }

        public void Vibrate(float leftVibration = 1f, float rightVibration = 1f, float time = 1.0f)
        {
            if (m_usingController && m_canVibrate)
            {
                m_vibrationTime = time;
                m_leftMotorVibration = leftVibration;
                m_rightMotorVibration = rightVibration;
                m_vibrating = true;
            }
        }

        public void StopVibration()
        {
            GamePad.SetVibration(0, 0, 0);
            m_vibrating = false;
            m_vibrationTime = 0.0f;
        }

        public bool SnapInput()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.O))
            {
                m_usingController = false;
                return true;
            }

            if (m_bIsControllerConnected)
            {
                if (prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed)
                {
                    m_usingController = true;
                    return true;
                }
            }

            return false;
        }

        public bool FluteInput()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                m_usingController = false;
                return true;
            }

            if (m_bIsControllerConnected)
            {
                if (prevState.Buttons.B == ButtonState.Released && state.Buttons.B == ButtonState.Pressed)
                {
                    m_usingController = true;
                    return true;
                }
            }

            return false;
        }

        public bool PesterBallInput()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                m_usingController = false;
                return true;
            }

            if (m_bIsControllerConnected)
            {
                if (prevState.Buttons.X == ButtonState.Released && state.Buttons.X == ButtonState.Pressed)
                {
                    m_usingController = true;
                    return true;
                }
            }

            return false;
        }

        public bool ThrowAppleInput()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                m_usingController = false;
                return true;
            }

            if (m_bIsControllerConnected)
            {
                if (prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed)
                {
                    m_usingController = true;
                    return true;
                }
            }

            return false;
        }

        public bool PauseInput()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                m_usingController = false;
                return true;
            }

            if (m_bIsControllerConnected)
            {
                if (prevState.Buttons.Start == ButtonState.Released && state.Buttons.Start == ButtonState.Pressed)
                {
                    m_usingController = true;
                    return true;
                }
            }

            return false;
        }

        public bool PointDownInput()
        {
            if (Input.GetButton("Pointing") || Input.GetKey(KeyCode.P))
            {
                m_usingController = false;
                return true;
            }

            if (m_bIsControllerConnected)
            {
                if (state.Triggers.Left < 0)
                {
                    m_usingController = true;
                    return true;
                }
            }

            return false;
        }

        public bool PointUpInput()
        {
            if (!m_usingController && CrossPlatformInputManager.GetButtonUp("Pointing"))
            {
                return true;
            }

            if (m_usingController && state.Triggers.Left > -0.1)
            {
                return true;
            }

            return false;
        }

        public bool RunDownInput()
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                m_usingController = false;
                return true;
            }

            if (m_bIsControllerConnected)
            {
                if (state.Triggers.Right > 0)
                {
                    m_usingController = true;
                    return true;
                }
            }

            return false;
        }

        public bool RunUpInput()
        {
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                m_usingController = false;
                return true;
            }

            if (m_usingController && state.Triggers.Right < 0.1)
            {
                return true;
            }

            return false;
        }
    }

}