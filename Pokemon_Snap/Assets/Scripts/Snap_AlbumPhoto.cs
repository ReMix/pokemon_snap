﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

namespace PokemonSnapUnity
{
    /// <summary>
    /// AlbumPhoto is almost the same as Snap_Photo, but this data is stored in Snap_AlbumManager.
    /// Variables are public for serialization (JSON)
    /// </summary>
    [System.Serializable]
    public class Snap_AlbumPhoto
    {
        #region Variables
        /// <summary>
        /// Unique Key for AlbumManager Dictionary
        /// </summary>
        public string m_key = "TEST";
        /// <summary>
        /// If this photo shows a Signal 
        /// </summary>
        public bool m_isSignal = false;
        /// <summary>
        /// If this photo is for a new discovered Pokémon
        /// </summary>
        public bool m_discovered = false;
        /// <summary>
        /// Pokémon number (25 = Pikachu...)
        /// </summary>
        public int m_albumNumber = 0;
        /// <summary>
        /// Name Showed in album
        /// </summary>
        public string m_name = "Test";
        /// <summary>
        /// Points got from the photo
        /// </summary>
        public float m_points = 0;
        #endregion

        #region Constructor
        public Snap_AlbumPhoto(JSONNode data)
        {
            m_key = data["Key"];
            m_discovered = data["Discovered"].AsBool;
            m_albumNumber = data["Number"].AsInt;
            m_name = data["Name"];
            m_points = data["Points"].AsFloat;
            m_isSignal = data["IsSignal"].AsBool;
        }

        public Snap_AlbumPhoto(int _num, string _name, bool _discovered, float _points, bool _isSignal)
        {
            m_isSignal = _isSignal;
            m_albumNumber = _num;
            m_name = _name;
            m_discovered = _discovered;
            m_points = _points;
        }

        public Snap_AlbumPhoto(int _num, string _name, bool _discovered, float _points, bool _isSignal, string _key)
        {
            m_key = _key;
            m_isSignal = _isSignal;
            m_albumNumber = _num;
            m_name = _name;
            m_discovered = _discovered;
            m_points = _points;
        }
        #endregion
    }
}