﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonSnapUnity
{
    public class Snap_PesterBall : MonoBehaviour
    {
        [SerializeField] private GameObject m_particleSystem;
        [SerializeField] private MeshRenderer m_renderer;
        [SerializeField] private Rigidbody m_rigidbody;
        [SerializeField] private Collider m_collider;
        [SerializeField] private AudioSource m_audioSource;
        [SerializeField] private AudioClip m_pesterSound;

        [SerializeField] private string m_playerTag = "Player";
        [SerializeField] private float m_timeToDeactivate = 2f;
        private float m_timer = 0f;
        private float m_destroyTimer = 0f;
        private bool m_thrown = false;

        public void Throw(Vector3 force)
        {
            m_rigidbody.AddForce(force);
            m_collider.enabled = true;
            m_thrown = true;
        }

        void Update()
        {
            if (m_thrown)
            {
                if (transform.position.y < -50)
                {
                    m_rigidbody.velocity = Vector3.zero;
                    m_timer = 0f;
                    m_renderer.enabled = true;
                    m_particleSystem.SetActive(false);
                    m_thrown = false;
                    this.gameObject.SetActive(false);
                    
                }
            }
        }

        IEnumerator destroyable(float time)
        {
            StopAllCoroutines();
            float timer = 0;
            while (timer < time)
            {
                m_timer += Time.deltaTime;
                yield return null;
            }

            m_timer = 0f;
            m_renderer.enabled = true;
            m_particleSystem.SetActive(false);
            m_thrown = false;
            this.gameObject.SetActive(false);
        }

        IEnumerator Timer()
        {
            StopAllCoroutines();
            m_audioSource.PlayOneShot(m_pesterSound);
            m_renderer.enabled = false;
            m_particleSystem.SetActive(true);
            m_rigidbody.velocity = Vector3.zero;

            while (m_timer < m_timeToDeactivate)
            {
                m_timer += Time.deltaTime;
                yield return null;
            }

            m_timer = 0f;
            m_renderer.enabled = true;
            m_particleSystem.SetActive(false);
            m_rigidbody.velocity = Vector3.zero;
            this.gameObject.SetActive(false);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.tag != m_playerTag)
            {
                m_collider.enabled = false;
                StartCoroutine(Timer());
            }
        }
    }

}
