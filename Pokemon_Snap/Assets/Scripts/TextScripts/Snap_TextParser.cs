﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using UnityEngine;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace PokemonSnapUnity
{

    /// <summary>
    /// XML and JSON Parser.
    /// </summary>
    public class Snap_TextParser
    {
        #region Variables
        /// <summary>
        ///  Dictionary of dictionaries so multiple languages can be used
        /// </summary>
        private Dictionary<string, Dictionary<string, Snap_InfoText>> m_vInfoTextItems;    
        /// <summary>
        /// Temporal dictionary for parsing data
        /// </summary>
        Dictionary<string, Snap_InfoText> m_vTempDictionary;
        /// <summary>
        /// Current language (String)
        /// </summary>
        private string m_languageKey = "";
        /// <summary>
        /// Current language (Unity enumeration)
        /// </summary>
        private SystemLanguage m_language;
        /// <summary>
        /// Folder where the data to read will be placed
        /// </summary>
        private string m_textFolderName = "GameData/Languages";
        /// <summary>
        /// Full path
        /// </summary>
        private string path;
        /// <summary>
        /// if the data can be parsed or not
        /// </summary>
        private bool m_canParse = true;
        #endregion

        #region Init
        /// <summary>
        /// Constructor
        /// </summary>
        public Snap_TextParser()
        {
            m_vTempDictionary = new Dictionary<string, Snap_InfoText>();
            m_vInfoTextItems = new Dictionary<string, Dictionary<string, Snap_InfoText>>();
            m_language = SystemLanguage.English;
            m_languageKey = m_language.ToString();
            path = Application.dataPath + "/Resources/" + m_textFolderName;

            if (!Directory.Exists(path))
            {
                m_canParse = false;
                Debug.LogError("Text Script Folder is not in game assets");
            }
        }
        #endregion

        #region Parse Data
        /// <summary>
        /// Set the language to desired
        /// </summary>
        /// <param name="language"></param>
        public void SetLang(SystemLanguage language)
        {
            m_languageKey = language.ToString();
            m_language = language;
        }
        /// <summary>
        /// Get language string
        /// </summary>
        /// <returns></returns>
        public string GetLang()
        {
            return m_languageKey;
        }

        /// <summary>
        /// Parse a XML Text Asset
        /// </summary>
        /// <param name="GameScript"></param>
        public void ParseScript(TextAsset GameScript)
        {
            m_vTempDictionary = new Dictionary<string, Snap_InfoText>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(GameScript.text);

            string language = "English";
            XmlNodeList langTable = xmlDoc.GetElementsByTagName("Language");

            foreach (XmlNode lang in langTable)
            {
                language = lang.InnerText;
            }

            if (m_vInfoTextItems.ContainsKey(language))
            {
                Debug.LogError("[UHMLXMLParse] Error! The Language should always be unique");
                Debug.LogError("Name: " + language);
                return;
            }

            XmlNodeList globalTable = xmlDoc.GetElementsByTagName("Table1");

            foreach (XmlNode TableNode in globalTable)
            {
                string newItemContext = TableNode.ChildNodes[0].InnerText;
                if (newItemContext != "")
                {
                    if (m_vTempDictionary.ContainsKey(newItemContext))
                    {
                        Debug.LogError("[UHMLXMLParse] Error! The context should always be unique (Script line " + TableNode.ChildNodes[0] + ")");
                        Debug.LogError("Name: " + newItemContext);
                    }
                    else
                    {
                        Snap_InfoText newInfoText = null;
                        //(string _key, string _text, string _audioName, bool _hasOptions, bool _showOptions)  
                        newInfoText = new Snap_InfoText(newItemContext,
                                                        TableNode.ChildNodes[1].InnerText,
                                                        TableNode.ChildNodes[3].InnerText,
                                                        (TableNode.ChildNodes[2].InnerText.Equals("Yes")) ? true : false);                        
                        m_vTempDictionary.Add(newItemContext, newInfoText);
                    }
                }
            }

            m_vInfoTextItems.Add(language, m_vTempDictionary);
            //Debug.Log("Script xml has been successfuly loaded with language: " + language);
        }
        #endregion

        #region Return Data
        /// <summary>
        /// Say Phrase (Needed for parsing some stuff)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Snap_InfoText SayPhrase(string context)
        {
            if (m_vInfoTextItems[m_languageKey].ContainsKey(context))
            {
                //Debug.Log("Saying "+context + "phrase: " + m_vInfoTextItems[context].Text);
                return m_vInfoTextItems[m_languageKey][context];
            }
            else
            {
                Debug.LogError("[UHMXMLParse] Error!! The phrase " + context + " doesn't exist in the dictionary of " + m_languageKey);
                return null;
            }
        }
        /// <summary>
        /// Almost the same than SayPhrase. used for parsing buttons and stuff
        /// </summary>
        /// <param name="scriptName"></param>
        /// <returns></returns>
        public string getTextInfo(string scriptName)
        {
            if (m_vInfoTextItems[m_languageKey].ContainsKey(scriptName))
            {
                //Debug.Log(m_languageKey+":"+ m_vInfoTextItems[m_languageKey][scriptName].Text);
                return m_vInfoTextItems[m_languageKey][scriptName].Text;
            }
            else
            {
                Debug.LogError("[UHMXMLParse] Error!! The phrase " + scriptName + " doesn't exist in the dictionary");
                return "";
            }
        }
        #endregion
    }

}