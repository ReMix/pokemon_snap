﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using UnityEngine;
using System.Collections.Generic;

namespace PokemonSnapUnity
{

    /// <summary>
    /// Text Info (Stored in Parser)
    /// </summary>
    public class Snap_InfoText
    {
        #region Variables
        private string m_sKey;          // Dictionary Key - Must be unique
        private string m_sText;         // Main Text 
        private string m_sAudioName;    // Name of the audio asociated to the text so it can has voice dialogues
        private bool m_bHasOptions;     // If it shows options
        int m_iNumberOfRows = 1;        // Number of rows. Need to press Interact Button to continue (From SnapManager)
        List<string> m_vTextRows;       // Text divided by rows

        //properties.
        public string Context { get { return m_sKey; } }
        public string Text { get { return m_sText; } }
        public string AudioName { get { return m_sAudioName; } }
        public bool HasOptions { get { return m_bHasOptions; } }
        public int NumberOfRows { get { return m_iNumberOfRows; } }
        public List<string> TextRows { get { return m_vTextRows; } }
        #endregion

        #region Initialization
        public Snap_InfoText(string _key, string _text, string _audioName, bool _hasOptions)
        {
            m_sKey = _key;
            m_sText = _text;
            m_sAudioName = _audioName;
            m_bHasOptions = _hasOptions;

            m_vTextRows = new List<string>();
            CalcNumberOfRows();
        }

        /// <summary>
        /// Calculates the rows based on /n in the text and save it in a list
        /// </summary>
        void CalcNumberOfRows()
        {
            string provString = "";
            char[] charArray = m_sText.ToCharArray();

            for (int i = 0; i < m_sText.Length; i++)
            {
                // Checks if a new row is needed
                if (i + 1 <= m_sText.Length && charArray[i] == '&' && charArray[i + 1] == 'n' && provString != "")
                {
                    m_iNumberOfRows++;
                    m_vTextRows.Add(provString);
                    provString = "";
                    i++;
                }
                // Doesn't add ""
                else if (charArray[i] == '"')
                {
                    continue;
                }
                //Añade el char actual al string provisional.
                else
                {
                    provString += charArray[i];
                }
            }
            // The line must not be null
            if (provString != "")
                m_vTextRows.Add(provString);
        }
        #endregion
    }

}