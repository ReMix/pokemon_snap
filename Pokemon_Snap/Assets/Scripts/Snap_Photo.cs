﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Snap_Photo is the class used by PhotoManager for storing Gameplay Data. 
    /// This is assigned to PhotoBase prefab with the right data
    /// </summary>
    public class Snap_Photo : MonoBehaviour
    {
        #region Variables
        [Header("~ Components ~"), Space(10)]
        /// <summary>
        /// Raw Image component that shows the pic done
        /// </summary>
        [SerializeField] private RawImage m_photoTexture;
        /// <summary>
        /// When the player does a pic to nothing, this blackscreen will be shown
        /// </summary>
        [SerializeField] private GameObject m_blackScreen;
        /// <summary>
        /// New Icon that shows when a Pokémon is Discovered
        /// </summary>
        [SerializeField] private GameObject m_newIndicator;
        /// <summary>
        /// For having the word "NEW" translated, we need to parse the text.
        /// </summary>
        [SerializeField] private Text m_newText;
        /// <summary>
        /// When a pic is selected in Selection Screen for Oak's Evaluation, this icon will be shown
        /// </summary>
        [SerializeField] private GameObject m_oakMark;
        /// <summary>
        /// When a pic is selected in Selection Screen for Album Storage, this icon will be shown
        /// </summary>
        [SerializeField] private GameObject m_albumMark;

        [Header("~ General Values ~"), Space(10)]
        [SerializeField] private string m_pokemonKey = "Null";
        /// <summary>
        /// Pokémon Name - We need this to know what Pokémon is being photographied
        /// </summary>
        [SerializeField] private string m_pokemonName = "Null";
        /// <summary>
        /// Total points got from the plus of every category
        /// </summary>
        [SerializeField] private float m_totalPoints = 0;
        /// <summary>
        /// If the Pokémon is being discovered in this course. 
        /// </summary>
        [SerializeField] private bool m_discoveredPokemon = false;
        /// <summary>
        /// If the pic snapped a signal
        /// </summary>
        [SerializeField] private bool m_isSignal = false;

        [Header("~ Size Values ~"), Space(10)]
        /// <summary>
        /// Minimum distance where the player can take the photo
        /// </summary>
        [SerializeField] private float m_minDistance = 0;
        /// <summary>
        /// Maximum distance where the player can take the photo 
        /// </summary>
        [SerializeField] private float m_maxDistance = 5f;
        /// <summary>
        /// Points the player got based on the distance to the Pokémon
        /// </summary>
        [SerializeField] private float m_sizePoints;
        /// <summary>
        /// Maximum points the player can get with size bonus
        /// </summary>
        [SerializeField] private float m_sizeMaxPoints = 1000f;

        [Header("~ Pose Values ~"), Space(10)]
        /// <summary>
        ///  Points the player got based on the pose to the Pokémon
        /// </summary>
        [SerializeField] private float m_posePoints;
        /// <summary>
        /// Maximum points the player can get with pose bonus
        /// </summary>
        [SerializeField] private float m_poseMaxPoints = 1000f;
        /// <summary>
        /// Minimum points the player can get without pose bonus
        /// </summary>
        [SerializeField] private float m_poseMinPoints = 500f;

        [Header("~ Technique Values ~")]
        /// <summary>
        ///  Points the player got based on the technique
        /// </summary>
        [SerializeField]
        private float m_techniquePoints;
        /// <summary>
        /// Maximum points the player can get with tech bonus
        /// </summary>
        [SerializeField] private float m_techniqueMaxPoints = 1000f;
        /// <summary>
        /// Relative distance to center
        /// </summary>
        [SerializeField] private float m_distanceToCenter;
        /// <summary>
        /// Range in which the distance to center means it is 
        /// </summary>
        [SerializeField] private float m_maxDistanceToCenter;

        [Header("~ Other Pokémon Values ~"), Space(10)]
        /// <summary>
        ///  Points the player got based on the pose to the Pokémon
        /// </summary>
        [SerializeField]
        private float m_otherPokemonPoints;
        /// <summary>
        /// Maximum points the player can get with pose bonus
        /// </summary>
        [SerializeField] private float m_otherPokemonMaxPoints = 1000f;
        /// <summary>
        /// If there are more Pokémon
        /// </summary>
        [SerializeField] private bool m_thereAreMorePokemon = false;
        /// <summary>
        /// If there are more Pokémon AND they are the same (E.G: two pikachu)
        /// </summary>
        [SerializeField] private bool m_thereAreMoreSamePokemon = false;
        /// <summary>
        /// Bonus gained if more pokemon are in the pic
        /// </summary>
        [SerializeField] private float m_otherSamePokemonMaxPoints = 1000f;

        [Header("~ Special Bonu Values ~"), Space(10)]
        /// <summary>
        /// If the player has used pester ball or something
        /// </summary>
        [SerializeField]
        private bool m_specialBonus = false;
        /// <summary>
        /// Points gained if having bonus
        /// </summary>
        [SerializeField] private float m_pointsPerBonus = 1000f;
        [SerializeField] private float m_bonusPoints = 0f;
        [SerializeField] private string m_bonusPhrase = "Daaaamn, boy";
        [SerializeField] private int m_photoNumber = 0;

        /// <summary>
        /// Properties
        /// </summary>
        public string PokemonKey { get { return m_pokemonKey; } }
        public string PokemonName { get { return m_pokemonName; } }
        public float TotalPoints { get { return m_totalPoints; } }
        public float SizePoints { get { return m_sizePoints; } }
        public float PosePoints { get { return m_posePoints; } }
        public float TechPoints { get { return m_techniquePoints; } }
        public float BonusPoints { get { return m_bonusPoints; } }
        public float SamePoints { get { return m_otherSamePokemonMaxPoints; } }
        public float OtherPoints { get { return m_otherPokemonPoints; } }
        public string BonusPhrase { get { return m_bonusPhrase; } }
        public RawImage PhotoTexture { get { return m_photoTexture; } }
        public bool NewPokemon { get { return m_discoveredPokemon; } }
        public int PhotoNumber { get { return m_photoNumber; } set { m_photoNumber = value; } }
        public bool IsSignal { get { return m_isSignal; } set { m_isSignal = value; } }
        #endregion

        #region MonoBehaviour
        private void Start()
        {
            m_newText.text = Snap_DialogManager.Instance.ParseXML.getTextInfo("newIcon");
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor (For null pics)
        /// </summary>
        public void SetPhotoDataNull()
        {
            m_pokemonKey = "Null";
            m_pokemonName = "MissingPKMN";
            m_blackScreen.SetActive(true);
        }

        /// <summary>
        /// Constructor with all the stuff needed
        /// </summary>
        /// <param name="_pokemonKey"></param>
        /// <param name="_pokemonName"></param>
        /// <param name="_distanceToPokemon"></param>
        /// <param name="_poseMultiplier"></param>
        /// <param name="_distanceToCenter"></param>
        /// <param name="_morePokemon"></param>
        /// <param name="_moreSamePokemon"></param>
        /// <param name="_specialBonus"></param>
        /// <param name="_newPokemon"></param>
        /// <param name="_photoNumber"></param>
        /// <param name="_isSignal"></param>
        public void SetPhotoData(string _pokemonKey, string _pokemonName, float _distanceToPokemon, float _poseMultiplier, float _distanceToCenter, bool _morePokemon, bool _moreSamePokemon, bool _specialBonus, bool _newPokemon, int _photoNumber, bool _isSignal, float _bonusPts)
        {
            m_pokemonKey = _pokemonKey;
            m_photoNumber = _photoNumber;
            m_pokemonName = _pokemonName;
            m_thereAreMorePokemon = _morePokemon;
            m_thereAreMoreSamePokemon = _moreSamePokemon;
            m_specialBonus = _specialBonus;
            m_discoveredPokemon = _newPokemon;

            m_pointsPerBonus = _bonusPts;

            m_isSignal = _isSignal;

            if (_newPokemon)
            {
                m_newIndicator.SetActive(true);
            }

            m_sizePoints = (int)((_distanceToPokemon / m_maxDistance) * m_sizeMaxPoints);


            m_posePoints = (int)(m_poseMinPoints * _poseMultiplier);


            m_techniquePoints = _distanceToCenter;

            if (m_thereAreMorePokemon)
            {

                m_otherPokemonPoints += m_otherPokemonMaxPoints;

                if (m_thereAreMoreSamePokemon)
                {
                    m_otherPokemonPoints = m_otherSamePokemonMaxPoints;
                }

                m_otherPokemonPoints = (int)m_otherPokemonPoints;
            }

            m_totalPoints = ((m_sizePoints + m_posePoints) * m_techniquePoints) + m_otherPokemonPoints;

            if (m_specialBonus)
            {
                m_totalPoints += m_pointsPerBonus;
            }

            m_totalPoints = (int)m_totalPoints;
        }
        #endregion

        #region Photo Selection
        /// <summary>
        /// Sends this instance to photo manager
        /// </summary>
        public void SelectThisPhoto()
        {
            Snap_PhotoManager.Instance.ShowSelectionPhotoPanel(this);
        }

        #endregion
    }
}