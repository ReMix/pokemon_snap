﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

namespace PokemonSnapUnity
{
    /// <summary>
    /// Interface that allows to Interact with stuff
    /// </summary>
    public interface ActionableObject
    {
        /// <summary>
        /// We just need to call to All ActionableObject with the overrided method OnAction
        /// </summary>
        void OnAction();
    }

}
