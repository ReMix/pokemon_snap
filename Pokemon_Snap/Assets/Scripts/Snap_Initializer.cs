﻿/// This code is personal and can't be used for commercial games.
/// Non-profit projects can use it with explicit permission and credits
/// 
/// Made by Manuel Rodríguez Matesanz.
/// You can contact me on GBATemp as Manurocker95, email: Manuelrodriguezmatesanz@gmail.com
/// or even on my web: Manuelrodriguezmatesanz.com
/// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PokemonSnapUnity
{
    /// <summary>
    /// Initializer called ONLY in InitializeManager Scene for INITIALIZING ALL NEEDED MANAGERS
    /// </summary>
    public class Snap_Initializer : MonoBehaviour
    {
        #region Variables (Prefabs)
        [Header("Settings Manager")]
        [SerializeField]
        private GameObject m_settingsManagerPrefab;

        [Header("Input Manager")]
        [SerializeField]
        private GameObject m_InputManagerPrefab;

        [Header("Audio Manager")]
        [SerializeField]
        private GameObject m_AudioManagerPrefab;

        [Header("Album Manager")]
        [SerializeField]
        private GameObject m_AlbumManagerPrefab;
        #endregion

        #region Init
        // Use this for initialization
        void Start()
        {
            if (m_InputManagerPrefab && !Snap_InputManager.Instance)
                Instantiate(m_InputManagerPrefab);

            if (m_settingsManagerPrefab && !Snap_SettingsManager.Instance)
                Instantiate(m_settingsManagerPrefab);

            if (m_AudioManagerPrefab && !Snap_AudioManager.Instance)
                Instantiate(m_AudioManagerPrefab);

            if (m_AlbumManagerPrefab && !Snap_AlbumManager.Instance)
                Instantiate(m_AlbumManagerPrefab);

            LoadScene();
        }
        #endregion

        #region Load Scene

        public IEnumerator LoadSceneCoroutine(int level)
        {
            AsyncOperation alevel = SceneManager.LoadSceneAsync(level, LoadSceneMode.Single);   //SceneManager.LoadScene(level, LoadSceneMode.Single);
            alevel.allowSceneActivation = true;

            while (!alevel.isDone)
            {
                //Debug.Log("%:"+ alevel.progress);
                yield return null;
            }
        }


        void LoadScene()
        {
            StartCoroutine(LoadSceneCoroutine(1));
        }

        #endregion
    }

}
